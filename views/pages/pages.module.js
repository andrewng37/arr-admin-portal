(function() {
  "use strict";

  angular
    .module("app.pages", [
			"ui.router", 
			"app.pages.orders",
      "app.pages.products",
      "app.pages.promotions",
      "app.pages.configuration"
            
		])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise("/dashboard");

    $stateProvider.state("app.sample", {
      url: "/sample",
      templateUrl: "views/pages/sample.html",
      ncyBreadcrumb: {
        label: "Sample"
      }
    });
  }
})();
