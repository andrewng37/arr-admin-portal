(function() {
  angular
    .module("app")
    .controller("NotiEditCtrl", function(
      $scope,
      $stateParams,
      $http,
      $state,
      $timeout
    ) {
      var id = $stateParams.id;

      $scope.id = parseInt(id);

      tinymce.init({
        selector: '#mytextarea'
      });
      
			$http.get("/src/js/data.json").then(
        function(response) {
          $scope.data = response.data;
          $scope.notifications = $scope.data.notifications;
          $scope.notifications.forEach(function(notification) {
            if ($scope.id === notification.id) {
              $scope.notification = notification;
            }
          }, this);
        },
        function(error) {
          console.log(error);
        }
      );

      
      
    });
})();
