angular
  .module("app")
  .controller("NotificationCtrl", function(
    $state,
    $scope,
    $stateParams,
    $http
  ) {
    // get data
    $http({
      method: "get",
      url: "/src/js/data.json"
    }).then(
      function(response) {
        $scope.data = response.data;
        $scope.notifications = $scope.data.notifications;
      },
      function(error) {
        console.log(error, "can not get data.");
      }
    );
  });
