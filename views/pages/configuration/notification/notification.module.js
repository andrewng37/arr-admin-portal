(function() {
    angular
      .module("app.pages.configuration.notification", [
        "ui.router",
  
        "app.pages.configuration.notification.list",
        "app.pages.configuration.notification.edit"
      ])
      .config(routeConfig);
  
    function routeConfig(
      $stateProvider,
      $urlRouterProvider,
      $ocLazyLoadProvider
    ) {
      $ocLazyLoadProvider.config({
        debug: true
      });
  
      $urlRouterProvider.otherwise("A");
  
      $stateProvider
        .state("app.configuration.notification.list", {
          url: "/list",
          templateUrl: "views/pages/configuration/notification/list/list-notification.html",
          controller: "NotificationCtrl",
          ncyBreadcrumb: {
            label: "List Notification"
          },
          params: {}
        })
        .state("app.configuration.notification.edit", {
          url: "/edit/:id",
          templateUrl: "views/pages/configuration/notification/edit/edit-notification.html",
          controller: "NotiEditCtrl",
          ncyBreadcrumb: {
            label: "Edit Notification"
          },
          params: {
            id: null
          }
        });
    }
  })();