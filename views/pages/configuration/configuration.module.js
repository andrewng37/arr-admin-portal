(function() {
    angular
      .module("app.pages.configuration", [
        "ui.router",
        "app.pages.configuration.notification",
        "app.pages.configuration.kiosk",
      ])
      .config(routeConfig);
  
    function routeConfig($stateProvider) {
      $stateProvider
        .state("app.configuration", {
          url: "/configuration",
          abstract: true,
          templateUrl: "views/pages/configuration/configuration_template.html",
          ncyBreadcrumb: {
            label: "Configuration"
          }
        })
        .state("app.configuration.notification", {
          url: "/notification",
          // controller: "OrdersListCtrl",
          templateUrl: "views/pages/configuration/notification/notification.html",
          ncyBreadcrumb: {
            label: "Notification"
          },
          params: {
            subtitle:
              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
          },
          resolve: {
            loadMyCtrl: [
              "$ocLazyLoad",
              function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  files: [
                    "vendors/js/jquery.dataTables.min.js",
                    "dist/datatables_extensions.min.js"
                  ]
                });
              }
            ]
          }
        }).state("app.configuration.kiosk", {
          url: "/kiosk",
          templateUrl: "views/pages/configuration/kiosk/kiosk.html",
          params: { },
          controller: "KioskCtrl",
          ncyBreadcrumb: {
              label: "Kiosk Display"
          }
      });
    }
  })();
