(function() {
    angular
        .module("app.pages.configuration.kiosk", [
            "ui.router",

        ])
        .config(routeConfig);

    function routeConfig(
        $stateProvider,
        $urlRouterProvider,
        $ocLazyLoadProvider
    ) {
        $ocLazyLoadProvider.config({
            debug: true
        });

        $urlRouterProvider.otherwise("A");

        //$stateProvider
        //    .state("app.configuration.notification", {
        //        url: "/notification",
        //        templateUrl: "views/pages/configuration/notification/notification.html",
        //        params: { },
        //        controller: "NotificationCtrl",
        //        ncyBreadcrumb: {
        //            label: "Notification"
        //        }
        //    });
    }
})();
