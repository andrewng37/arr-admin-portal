angular
    .module("app")
    .controller("KioskCtrl", function(
        $state,
        $scope,
        $stateParams,
        $http
    ) {
        // get data
        $http({
            method: "get",
            url: "/src/js/data.json"
        }).then(
            function(response) {

                $scope.data = response.data;
                $scope.notifications = $scope.data.notifications;
                console.log($scope.notifications);
            },
            function(error) {
                console.log(error, "can not get data.");
            }
        );

        $state.current.params.subtitle = 'Last edited by: Kelvin Tan, 18/08/2018, 12.03PM';
    });
