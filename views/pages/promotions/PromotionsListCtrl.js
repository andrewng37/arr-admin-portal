//orders.js
angular.module("app").controller("PromotionsListCtrl", PromotionsListCtrl);

PromotionsListCtrl.$inject = ["$scope", "$http", "$state"];

function PromotionsListCtrl($scope, $http, $state) {
    $http({
        method: "get",
        url: "src/js/data_promotions.json"
    }).then(
        function(response) {
            $scope.labels = response.data.labels;
            $scope.items = response.data.items;
        },
        function(error) {
            console.log(error, "There is something wrong with getting data.");
        }
    );

    $scope.toggleAll = () => {
        var toggleStatus = $scope.isAllSelected;
        $scope.items.forEach(function(item) {
            item.selected = toggleStatus;
        }, this);
    };

    $scope.optionToggled = () => {
        $scope.isAllSelected = $scope.items.every(function(item) {
            return item.selected;
        });
    };

    angular.element(document).ready(function() {
        $scope.table = angular.element("#lgt-tbl-promotions-list").DataTable({
            dom:
                '<"#lgt-export-btns.lgt-hidden"B>r<"#lgt-datatable-length"l><"#lgt-datatable-paginate"p>t',
            bDestroy: true,
            buttons: ["csv", "excel", "pdf", "print"],
            pagingType: "full_numbers",
            language: {
                lengthMenu: " _MENU_ ",
                paginate: {
                    first: '<i class="fa fa-step-backward">',
                    previous: '<i class="fa fa-caret-left">',
                    next: '<i class="fa fa-caret-right">',
                    last: '<i class="fa fa-step-forward">'
                }
            }
        });
    });
}
