(function() {
    angular
        .module("app.pages.promotions", [
            "ui.router",
            //
            // "app.pages.orders.order",
            // "app.pages.orders.delivery"
        ])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.promotions", {
                url: "/promotions",
                abstract: true,
                templateUrl: "views/pages/promotions/promotions_template.html",
                ncyBreadcrumb: {
                    label: "Promotions"
                }
            })
            .state("app.promotions.list", {
                url: "/list",
                controller: "PromotionsListCtrl",
                templateUrl: "views/pages/promotions/list.html",
                ncyBreadcrumb: {
                    label: "Promotion Management"
                },
                params: {
                    subtitle:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
                },
                resolve: {
                    loadMyCtrl: [
                        "$ocLazyLoad",
                        function($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                files: [
                                    "vendors/js/jquery.dataTables.min.js",
                                    "dist/datatables_extensions.min.js"
                                ]
                            });
                        }
                    ]
                }
            })
            .state("app.promotions.add", {
                url: "/add",
                controller: "PromotionsAddCtrl",
                templateUrl: "views/pages/promotions/add.html",
                ncyBreadcrumb: {
                    label: "Add New Promotion Code"
                },
                params: {
                    subtitle:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
                }
            })
            .state("app.promotions.modify", {
                url: "/modify/:id",
                controller: "PromotionsModifyCtrl",
                templateUrl: "views/pages/promotions/modify.html",
                ncyBreadcrumb: {
                    label: "Modify Promotion Code"
                },
                params: {
                    id: null,
                    subtitle:
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
                }
            })
            ;
    }
})();
