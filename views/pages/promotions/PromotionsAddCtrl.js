//orders.js
angular.module("app").controller("PromotionsAddCtrl", PromotionsAddCtrl)
    .directive('lgtPromotionsAddForm', lgtPromotionsAddForm);

PromotionsAddCtrl.$inject = ["$scope", "$http", "$state"];

function PromotionsAddCtrl($scope, $http, $state) {
    $state.current.data = {
        back: true,
        backto: "app.promotions.list"
    };

    $scope.showBtnDelete = false;

    $scope.promotionCode = {
        promoCode: null,
        description: null,
        discountValue: null,
        discountType: null,
        validFrom: null,
        validUntil: null,
        status: null,
        createdDate: '7 Aug 2018',
        createdBy: 'John',
        modifiedDate: '7 Aug 2018',
        modifiedBy: 'John'
    };

    angular.element(document).ready(function() {
        angular.element('#lgt-promotions-form').submit(function(e) {
            e.preventDefault();
        });
    });
}

function lgtPromotionsAddForm() {
    return {
        templateUrl: 'views/pages/promotions/promotions_form.html'
    }
}
