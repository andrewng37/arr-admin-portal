angular
  .module("app")
  .controller("PromotionsCtrl", function($scope, $stateParams, $http, $state) {
    var id = $stateParams.id;
    $scope.id = parseInt(id);

    $scope.actionExport = function(type) {
      switch (type) {
        case "csv":
          angular.element(".buttons-csv").click();
          break;
        case "excel":
          angular.element(".buttons-excel").click();
          break;
        case "pdf":
          angular.element(".buttons-pdf").click();
          break;
        case "print":
          angular.element(".buttons-print").click();
          break;
      }
    };

    $scope.showBtn = function(type) {
        switch(type) {
            case 'export':
                return $state.includes('app.promotions.list');
                break;
            default:
                return false;
                break;
        }
    }

  });