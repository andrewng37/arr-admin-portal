angular.module("app")
    .controller("PromotionsModifyCtrl", PromotionsModifyCtrl)
    .directive('lgtPromotionsModifyForm', lgtPromotionsModifyForm);

PromotionsModifyCtrl.$inject = ["$scope", "$http", "$state", "$stateParams"];

function PromotionsModifyCtrl($scope, $http, $state, $stateParams) {
    $scope.id = $stateParams.id;

    $state.current.data = {
        back: true,
        backto: "app.promotions.list"
    };

    $scope.showBtnDelete = true;

    $http.get('/src/js/data_promotions.json').then(function(res) {
        var items = res.data.items;
        items.forEach(function(value, key) {
            if (value.id == $scope.id) {
                $scope.promotionCode = value;
            }
        })
    }, function(err) {
        console.log('ERROR');
        console.log(err);
    });


    angular.element(document).ready(function() {
        angular.element('#lgt-promotions-form').submit(function(e) {
            e.preventDefault();
        });
    });

}

function lgtPromotionsModifyForm() {
    return {
        templateUrl: 'views/pages/promotions/promotions_form.html'
    }
}
