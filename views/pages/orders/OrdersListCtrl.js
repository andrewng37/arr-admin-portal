//orders.js
angular.module("app").controller("OrdersListCtrl", OrdersListCtrl);

OrdersListCtrl.$inject = ["$scope", "$http", "$state"];

function OrdersListCtrl($scope, $http, $state) {
  $http({
    method: "get",
    url: "http://nas-arr.ddns.net/OrderApi/OrderList"
  }).then(
    function(response) {
      $scope.orders = response.data;
      $scope.orders.forEach(function(order) {
        if (order.Created_Date) {
          $scope.ex =  order.Created_Date.slice(6, 19);
          order.Created_Date = new Date(parseInt($scope.ex));
        }
        if (order.Modified_Date) {
          $scope.mo =  order.Modified_Date.slice(6, 19);
          order.Modified_Date = new Date(parseInt($scope.mo));
        }
      }, this);
      $scope.collapseDetailsTable = function(orderid, index) {
        $http({
          method: "get",
          url: "http://nas-arr.ddns.net/Orderapi/ProductListByOrderID?Orderid="+orderid
        }).then(
          function(response) {
            
            $scope.items = response.data;
            
            $scope.orders.forEach(function(order) {
              
              if (order.Order_ID == orderid) {
                order.items = $scope.items;
              }
            }, this);
          },
          function(error) {
            console.log(error, "can not get data.");
          }
        );
      };
    },
    function(error) {
      console.log(error, "can not get data.");
    }
  );

  $scope.toggleAll = () => {
    var toggleStatus = $scope.isAllSelected;
    $scope.orders.forEach(function(itm) {
      itm.selected = toggleStatus;
    }, this);
  };

  $scope.optionToggled = () => {
    $scope.isAllSelected = $scope.orders.every(function(itm) {
      return itm.selected;
    });
  };
  

  

  // $scope.collapseDetailsTable = function(orderid, index) {
  //   // let tr = $(this).closest('tr');
  //   let tr = $("tr[data-orderid=" + orderid + "]");

  //   var row = $scope.table.row(tr);

  //   if (
  //     angular.element('.accordion-body[data-order-id="' + orderid + '"]').length
  //   ) {
  //     // row.child.hide();
  //     tr.removeClass("shown");
  //     if (
  //       angular
  //         .element('.accordion-body[data-order-id="' + orderid + '"]')
  //         .hasClass("collapse")
  //     ) {
  //       angular
  //         .element('.accordion-body[data-order-id="' + orderid + '"]')
  //         .removeClass("collapse");
  //       setTimeout(function() {
  //         angular
  //           .element('.accordion-body[data-order-id="' + orderid + '"]')
  //           .parents("tr")
  //           .remove();
  //       }, 300);
  //     } else
  //       angular
  //         .element('.accordion-body[data-order-id="' + orderid + '"]')
  //         .addClass("collapse");
  //   } else {
  //     // var html = $scope.format(orderid, index);
  //     // tr.after(html);

  //     row.child($scope.format(orderid, index)).show();
  //     tr.addClass("shown");
  //   }
  //   console.log(tr);
  // };

  // $scope.goTo = function(pr1, pr2) {
  //   $state.go("app.orders.order.edit", { id: pr1, id_item: pr2 });
  // };

  // $scope.format = function(orderid, index) {
    
  //   var html = "";
  //   var data = null;

  //   $scope.orders.forEach(function(item) {
  //     if (item.Order_ID == orderid) {
  //       data = item;
  //     }
  //   }, this);
  //   // data.items = [];
    

  //   // $scope.productbyOrders.forEach(function(productbyOrder) {
  //   //   if (productbyOrder.Order_ID == data.Order_ID) {
  //   //     data.items.push(productbyOrder);
  //   //   }
  //   // }, this);
  //   if (data == null) return false;

  //   if (data.items.length) {
  //     html +=
  //     '<div style="margin: 0px;" data-order-id="' +
  //     orderid +
  //     '" class="accordion-body collapse packageDetails-' +
  //     index +
  //     ' style-accordion accordion-body-list-order" id="accordion-' +
  //     index +
  //     '">' +
  //     '<table class="table lgt-tbl-package-details">' +
  //     "<thead>" +
  //     '<tr class="head-table-orders-items">' +
  //     "<th>Item</th>" +
  //     "<th>UUID</th>" +
  //     "<th>ACC NO.</th>" +
  //     "<th>Record Title (File Name)</th>" +
  //     "<th>Format</th>" +
  //     "<th>Record Status</th>" +
  //     "<th>Payment Status</th>" +
  //     "<th>Handled BY</th>" +
  //     "<th></th>" +
  //     "</tr>" +
  //     "</thead>" +
  //     "<tbody>";
      
  //     data.items.forEach(function(item) {
  //       html +=
  //         '<tr class="body-table-orders-items accordion-toggle">' +
  //         "<td>" +
  //         '00' + index++ +
  //         "</td>" +
  //         "<td>" +
  //         item.UUID +
  //         "</td>" +
  //         "<td>" +
  //         item.ACC_NO +
  //         "</td>" +
  //         "<td>" +
  //         item.Record_Title +
  //         "</td>" +
  //         "<td>" +
  //         item.Format +
  //         "</td>" +
  //         "<td>" +
  //         item.Record_Status +
  //         "</td>" +
  //         "<td>" +
  //         item.Payment_Status +
  //         "</td>" +
  //         "<td>" +
  //         item.HandLed_By +
  //         "</td>" +
  //         "<td>" +
  //         "<i class=\"zmdi zmdi-eye\" onclick=\"angular.element(this).scope().goTo('" +
  //         orderid +
  //         "','" +
  //         item.Order_Item_ID +
  //         '\')"></i>' +
  //         "</td>" +
  //         "</tr>";
  //     }, this);
  //   }

  //   html += "</tbody></table></div></td></tr>";

  //   return html;
  // };

  // angular.element(document).ready(function() {
  //   angular.element("#lgt-tbl-orders-list").DataTable({
  //     dom:
  //       '<"#lgt-export-btns.lgt-hidden"B>r<"#lgt-datatable-length"l><"#lgt-datatable-paginate"p>t',
  //     bDestroy: true,
  //     buttons: ["csv", "excel", "pdf", "print"],
  //     pagingType: "full_numbers",
  //     language: {
  //       lengthMenu: " _MENU_ ",
  //       paginate: {
  //         first: '<i class="fa fa-step-backward">',
  //         previous: '<i class="fa fa-caret-left">',
  //         next: '<i class="fa fa-caret-right">',
  //         last: '<i class="fa fa-step-forward">'
  //       }
  //     }
  //   });

    
  // });
}
