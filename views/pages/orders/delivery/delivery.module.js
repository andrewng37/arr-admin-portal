(function() {
  angular
    .module("app.pages.orders.delivery", [
      "ui.router",

      "app.pages.orders.delivery.list",
      "app.pages.orders.delivery.edit"
    ])
    .config(routeConfig);

  function routeConfig(
    $stateProvider,
    $urlRouterProvider,
    $ocLazyLoadProvider
  ) {
    $ocLazyLoadProvider.config({
      debug: true
    });

    $urlRouterProvider.otherwise("A");

    $stateProvider
      .state("app.orders.delivery.list", {
        url: "/list",
        templateUrl: "views/pages/orders/delivery/list/list.html",
        controller: "DeliveryCtrl",
        ncyBreadcrumb: {
          label: "Delivery Management"
        },
        params: {}
      })
      .state("app.orders.delivery.edit", {
        url: "/edit/:id",
        templateUrl: "views/pages/orders/delivery/edit/edit-delivery.html",
        controller: "DeliveryEditCtr",
        ncyBreadcrumb: {
          label: "Delivery Instruction Detail"
        },
        params: {
          id: null
        }
      })
      .state("app.orders.delivery.deliver", {
        url: "/delivery/deliver",
        templateUrl: "views/pages/orders/delivery/deliver/deliver.html",
        controller: "deliverCtrl",
        ncyBreadcrumb: {
          label: "Generate"
        },
        params: {
          obj: null,
          subtitle:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
        }
      });
  }
})();
