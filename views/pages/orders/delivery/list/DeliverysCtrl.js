angular
  .module("app")
  .controller("DeliveryCtrl", function($state, $scope, $stateParams, $http) {
    $http({
      method: "get",
      url: "http://nas-arr.ddns.net/DeliveryApi/DeliveryList"
    }).then(
      function(response) {
        $scope.data = response.data;
        $scope.deliverys = $scope.data;
        $scope.deliverys.forEach(function(delivery) {
          delivery.selected = false;
          if (delivery.Expiry_Date) {
            $scope.ex =  delivery.Expiry_Date.slice(6, 19);
            delivery.Expiry_Date = new Date(parseInt($scope.ex));
          }
          if (delivery.Created_On) {
            $scope.cre =  delivery.Created_On.slice(6, 19);
            delivery.Created_On = new Date(parseInt($scope.cre));
          }
        }, this);
      },
      function(error) {
        console.log(error, "can not get data.");
      }
    );

    $scope.toggleAll = () => {
      var toggleStatus = $scope.isAllSelected;
      $scope.deliverys.forEach(function(itm) {
        itm.selected = toggleStatus;
      }, this);
    };

    $scope.optionToggled = item => {

      $scope.isAllSelected = $scope.deliverys.every(function(itm) {
        return itm.selected;
      });
    };

    //   $scope.generate = () => {
    //       $state.go('app.orders.order.generate', { obj: $scope.itemss})
    //   }
  });
