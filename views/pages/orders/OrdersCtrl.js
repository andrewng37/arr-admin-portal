angular
  .module("app")
  .controller("OrdersCtrl", function($scope, $stateParams, $http, $state) {
    var id = $stateParams.id;
    $scope.id = parseInt(id);

    // get data
    $http({
      method: "get",
      url: "http://nas-arr.ddns.net/OrderApi/OrderList"
    }).then(
      function(response) {
        
        $scope.data = response.data;
        // $scope.orders = $scope.data;
        
        $scope.data.forEach(function(order) {
          if ($scope.id === order.id) {
            $scope.order = order;
          }
        }, this);
      },
      function(error) {
        console.log(error, "can not get data.");
      }
    );

    $scope.actionExport = function(type) {
      switch (type) {
        case "csv":
          angular.element(".buttons-csv").click();
          break;
        case "excel":
          angular.element(".buttons-excel").click();
          break;
        case "pdf":
          angular.element(".buttons-pdf").click();
          break;
        case "print":
          angular.element(".buttons-print").click();
          break;
      }
    };

    $scope.showBtn = function(type) {
        switch(type) {
            case 'generateQuotation':
                return $state.includes('app.orders.order.product')
                    || $state.includes('app.orders.order.info')
                    || $state.includes('app.orders.order.log');
                break;
            case 'deliver':
                return $state.includes('app.orders.order.product');
                break;
            default:
                return false;
                break;
        }
    }

  })
    .directive('lgtSearchFilter', lgtSearchFilter);

function lgtSearchFilter() {
    return {
        templateUrl: 'views/components/lgt-search-filter.html'
    }
}