(function() {
  angular
    .module("app.pages.orders", [
      "ui.router",

      "app.pages.orders.order",
      "app.pages.orders.delivery"
    ])
    .config(routeConfig);

  function routeConfig($stateProvider) {
    $stateProvider
      .state("app.orders", {
        url: "/orders",
        abstract: true,
        templateUrl: "views/pages/orders/orders_template.html",
        ncyBreadcrumb: {
          label: "Order"
        }
      })
      .state("app.orders.list", {
        url: "/list",
        controller: "OrdersListCtrl",
        templateUrl: "views/pages/orders/list.html",
        ncyBreadcrumb: {
          label: "Order Management"
        },
        params: {
          subtitle:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
        },
        resolve: {
          loadMyCtrl: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load({
                files: [
                  "vendors/js/jquery.dataTables.min.js",
                  "dist/datatables_extensions.min.js"
                ]
              });
            }
          ]
        }
      })
      .state("app.orders.delivery", {
        url: "/delivery",
        // controller: "OrdersListCtrl",
        templateUrl: "views/pages/orders/delivery/delivery.html",
        ncyBreadcrumb: {
          label: "Delivery Management"
        },
        params: {
          subtitle:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
        },
        resolve: {
          loadMyCtrl: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load({
                files: [
                  "vendors/js/jquery.dataTables.min.js",
                  "dist/datatables_extensions.min.js"
                ]
              });
            }
          ]
        }
      });
  }
})();
