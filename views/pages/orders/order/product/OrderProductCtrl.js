angular
  .module("app")
  .controller("OrderProductCtrl", function(
    $state,
    $scope,
    $stateParams,
    $http,
    $timeout
  ) {
    var id = $stateParams.id;
    $scope.id = id;
    $scope.items = [];
    $state.current.data = {
      back: true,
      backto: "app.orders.list"
    };
    // get data
    $http({
      method: "get",
      url:
        "http://nas-arr.ddns.net/Orderapi/ProductListByOrderID?Orderid=" +
        $scope.id
    }).then(
      function(res) {
        $scope.itemsShow = res.data;
        $scope.itemsShow.forEach(function(item) {
          if (item.Last_Modified) {
            $scope.mo = item.Last_Modified.slice(6, 19);
            item.Last_Modified = new Date(parseInt($scope.mo));
          }
        }, this);
      },
      function(error) {
        console.log(error, "can not get data.");
      }
    );

    $http({
      method: "get",
      url: "http://nas-arr.ddns.net/OrderApi/OrderList"
    }).then(
      function(res) {
        res.data.forEach(function(order) {
          if (order.Order_ID == $scope.id) {
            $scope.order = order;
          }
        }, this);

        $state.current.ncyBreadcrumb.label =
          "Edit Details - #" + $scope.order.Work_Order;
      },
      function(error) {
        console.log(error, "can not get data.");
      }
    );

    $scope.editOrder = (itemId) => {
      $state.go("app.orders.order.edit", { id: $scope.id, id_item: itemId });
    };

    $scope.toggleAll = items => {
      if ($scope.isAllSelected === true) {
        $scope.itemsData = items;
      } else {
        $scope.itemsData = [];
      }
      $scope.itemss = {
        selected: "true",
        items: $scope.itemsData
      };

      var toggleStatus = $scope.isAllSelected;
      $scope.itemsShow.forEach(function(itm) {
        itm.selected = toggleStatus;
      }, this);
    };

    $scope.optionToggled = item => {
      if (item.selected === true) {
        $scope.items.push(item);
      } else {
        $scope.items.splice(item, 1);
      }
      $scope.itemss = {
        selected: "true",
        items: $scope.items
      };
      $scope.isAllSelected = $scope.items.every(function(itm) {
        return itm.selected;
      });
    };

    $scope.returnRecordStatus = function(code) {
      switch (code.toUpperCase()) {
        case "PV":
          return "Pending Verification";
          break;
        case "PQ":
          return "Pending Quatation";
          break;
        case "PR":
          return "Pending Reproduction";
          break;
        case "VR":
          return "Verifying Request";
          break;
        case "C":
          return "Closed";
          break;
        case "CM":
          return "Completed";
          break;
        case "CN":
          return "Cancelled";
          break;
        case "CP":
          return "Search Result Found";
          break;
        case "CPU":
          return "Search Result Not Found";
          break;
        case "OP":
          return "Open";
          break;
        case "CPU":
          return "Search Result Not Found";
          break;
        case "PCL":
          return "Pending Collection";
          break;
        case "PD":
          return "Pending Generate Delivery";
          break;
        case "PP":
          return "Pending Search";
          break;
        case "PPY":
          return "Pending Payment";
          break;
        case "PD":
          return "Pending Generate Delivery";
          break;
        case "PVR":
          return "Pending Verification of Request";
          break;
        case "RR":
          return "Reproducing Record";
          break;
        case "SC":
          return "Searching Record";
          break;
        case "VR":
          return "Verifying Request";
          break;
        default:
          return "Unknown";
          break;
      }
    };

    $scope.hasProblem = function(record_status) {
      return ["PV", "PQ"].indexOf(record_status) !== -1;
    };

    $scope.generate = () => {
      $state.go("app.orders.order.generate", { obj: $scope.itemss });
    };

    $scope.deliver = () => {
      $state.go("app.orders.order.deliver", { obj: $scope.itemss });
    };

    angular.element(document).ready(function() {
      angular.element(".lgt-btn-generate-quotation").on("click", function(e) {
        e.preventDefault();
        $scope.generate();
      });

      angular.element(".lgt-btn-deliver").on("click", function(e) {
        e.preventDefault();
        $scope.deliver();
      });

      // angular.element("#tab_1 table#lgt-my-table").DataTable({
      //   dom:
      //     '<"#lgt-export-btns.lgt-hidden"B>r<"#lgt-datatable-length"l><"#lgt-datatable-paginate"p>t',
      //   bDestroy: true,
      //   buttons: ["csv", "excel", "pdf", "print"],
      //   pagingType: "full_numbers",
      //   language: {
      //     lengthMenu: " _MENU_ ",
      //     paginate: {
      //       first: '<i class="fa fa-step-backward">',
      //       previous: '<i class="fa fa-caret-left">',
      //       next: '<i class="fa fa-caret-right">',
      //       last: '<i class="fa fa-step-forward">'
      //     }
      //   }
      // });
    });
  });
