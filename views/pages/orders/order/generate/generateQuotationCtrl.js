(function() {
  angular
    .module("app")
    .controller("OrderGenerateCtr", function(
      $scope,
      $stateParams,
      $http,
      $state,
      $uibModal
    ) {
      $scope.title = $stateParams.subtitle;
      $scope.items = $stateParams.obj.items;
      // console.log($scope.items);

      $scope.openModal = function() {
        var modalInstance = $uibModal.open({
          templateUrl: "/src/views/pages/orders/order/generate/modalContent.html",
          controller: "ModalContentCtrl",
          size: ""
        });

        modalInstance.result.then(function(response) {
          $scope.result = `${response} button hitted`;
        });
      };
    })
    .controller("ModalContentCtrl", function($scope, $uibModalInstance) {
      $scope.ok = function() {
        $uibModalInstance.close("Ok");
      };

      $scope.cancel = function() {
        $uibModalInstance.dismiss();
      };
    });
})();
