(function() {
  angular.module("app.pages.orders.order.generate", [
    "ui.router",
    "ngAnimate",
    "ngSanitize",
    "ui.bootstrap"
  ]);
})();
