(function() {
  angular
    .module("app")
    .controller("OrderInfoCtrl", function($scope, $stateParams, $http, $state) {
      $scope.id = $stateParams.id;

      $state.current.data = {
        back: true,
        backto: "app.orders.list"
      };

      $http.get("http://nas-arr.ddns.net/OrderApi/OrderList").then(
        function(response) {
          $scope.data = response.data;
          $scope.data.forEach(function(order) {
            if (order.Order_ID == $scope.id) {
              $scope.data = order;
            }
          }, this);
          $state.current.ncyBreadcrumb.label =
            "Edit Details - #" + $scope.data.Work_Order;
        },
        function(error) {
          console.log(error);
        }
      );

      $scope.getInvoiceValue = function(key, value) {
        if (key == "itemIDs") {
          return value.join("/");
        } else return value;
      };

      $scope.getLabel = function(labelKey) {
        var obj = {
          Order_Status: "Order Status",
          Request_ID: "Request ID",
          Requestor_Name: "Requestor's Name",
          Requestor_Email: "Requestor's Email",
          Mobile_No: "Mobile No.",
          Payment_Method: "Payment Method",
          no: "Invoice No.",
          date: "Invoice Date",
          paymentStatus: "Payment Status",
          paymentDate: "Payment Date",
          amount: "Invoice Amount",
          itemIDs: "Item ID(s)",
          routing: "Route to"
        };

        return obj[labelKey];
      };
    });
})();
