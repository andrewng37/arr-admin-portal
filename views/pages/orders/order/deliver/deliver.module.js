(function() {
  angular.module("app.pages.orders.order.deliver", [
    "ui.router",
    "ngAnimate",
    "ngSanitize",
    "ui.bootstrap"
  ]);
})();
