(function () {

    angular.module('app').controller('OrderLogCtrl', function($scope, $stateParams, $http, $state) {
        $scope.id = $stateParams.id;

        $state.current.data = {
            back: true,
            backto: 'app.orders.list'
        };

        $http.get("/src/js/order_info.json")
            .then(function(response) {
                $scope.sampleData = response.data;

                $state.current.ncyBreadcrumb.label = 'Edit Details - #' + $scope.sampleData.generalInfomation.requestID;
            }, function (error) {
                console.log(error);
            });

    });

})();