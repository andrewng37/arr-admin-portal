(function() {
  angular
    .module("app.pages.orders.order", [
      "ui.router",

      "app.pages.orders.order.product",
      "app.pages.orders.order.info",
      "app.pages.orders.order.log",
      "app.pages.orders.order.edit",
      "app.pages.orders.order.generate",
      "app.pages.orders.order.deliver"
    ])
    .config(routeConfig);

  function routeConfig(
    $stateProvider,
    $urlRouterProvider,
    $ocLazyLoadProvider
  ) {
    $ocLazyLoadProvider.config({
      debug: true
    });

    $urlRouterProvider.otherwise("A");

    $stateProvider
      .state("app.orders.order", {
        abstract: true,
        url: "/:id",
        templateUrl: "views/pages/orders/order/order_template.html",
        params: { id: null },
        controller: "OrderCtrl",
        ncyBreadcrumb: {
          label: "Order"
        }
      })
      .state("app.orders.order.product", {
        url: "/product",
        templateUrl: "views/pages/orders/order/product/product.html",
        controller: "OrderProductCtrl",
        ncyBreadcrumb: {
          label: "Product"
        },
        params: {
          // 'id': null,
          subtitle:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
        },
        resolve: {
          loadMyCtrl: [
            "$ocLazyLoad",
            function($ocLazyLoad) {
              return $ocLazyLoad.load({
                files: [
                  "vendors/js/jquery.dataTables.min.js",
                  "dist/datatables_extensions.min.js"
                ]
              });
            }
          ]
        }
      })
      .state("app.orders.order.info", {
        url: "/info",
        templateUrl: "views/pages/orders/order/info/info.html",
        controller: "OrderInfoCtrl",
        ncyBreadcrumb: {
          label: "Info"
        },
        params: {
          subtitle:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
          // id: null
        }
      })
      .state("app.orders.order.log", {
        url: "/log",
        templateUrl: "views/pages/orders/order/log/log.html",
        controller: "OrderLogCtrl",
        ncyBreadcrumb: {
          label: "Log"
        },
        params: {
          subtitle:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
        }
      })
      .state("app.orders.order.edit", {
        url: "/edit/:id_item",
        templateUrl: "views/pages/orders/order/edit/edit.html",
        controller: "OrderEditCtr",
        ncyBreadcrumb: {
          label: "Edit"
        },
        params: {
          id: null,
          id_item: null
        }
      })
      .state("app.orders.order.generate", {
        url: "/generate/generate-quotation",
        templateUrl: "views/pages/orders/order/generate/generateQuotation.html",
        controller: "OrderGenerateCtr",
        ncyBreadcrumb: {
          label: "Generate"
        },
        params: {
          obj: null,
          subtitle:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
        }
      })
        .state("app.orders.order.deliver", {
            url: "/generate/generate-deliver",
            templateUrl: "views/pages/orders/order/deliver/deliver.html",
            controller: "deliverCtrl",
            ncyBreadcrumb: {
                label: "Generate Deliver"
            },
            params: {
                obj: null,
                subtitle:
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In porta vestibulum cursus."
            }
        });
  }
})();
