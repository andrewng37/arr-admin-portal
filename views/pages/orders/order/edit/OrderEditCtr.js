(function() {
  angular
    .module("app")
    .controller("OrderEditCtr", function($scope, $stateParams, $http, $state) {
      var id = $stateParams.id;
      // $scope.id = parseInt(id);
      var id_item = $stateParams.id_item;
      // $scope.id_item = parseInt(id_item);
      
      $http({
        method: "get",
        url: "http://nas-arr.ddns.net/Orderapi/ProductListByOrderID?Orderid="+ id
      }).then(
        function(res) {
          $scope.items = res.data;
          
          $scope.items.forEach(function(item) {
            if (item.Order_Item_ID == id_item) {
              $scope.item = item;
            }
          }, this);
        },
        function(error) {
          console.log(error, "can not get data.");
        }
      );

      $http({
        method: "get",
        url: "http://nas-arr.ddns.net/StatusApi/GetAllStatus"
      }).then(
        function(res) {
          $scope.status = res.data;
        },
        function(error) {
          console.log(error, "can not get data.");
        }
      );


      $scope.getInvoiceValue = function(key, value) {
        if (key == "itemIDs") {
          return value.join("/");
        } else return value;
      };

      $scope.getLabel = function(labelKey) {
        var obj = {
          orderStatus: "Order Status",
          requestID: "Request ID",
          requestorName: "Requestor's Name",
          requestorEmail: "Requestor's Email",
          mobile: "Mobile No.",
          paymentMethod: "Payment Method",
          no: "Invoice No.",
          date: "Invoice Date",
          paymentStatus: "Payment Status",
          paymentDate: "Payment Date",
          amount: "Invoice Amount",
          itemIDs: "Item ID(s)"
        };

        return obj[labelKey];
      };
    });
})();
