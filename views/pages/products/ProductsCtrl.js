angular
.module("app")
.controller("ProductsCtrl", function($scope, $stateParams, $http, $state) {
  var id = $stateParams.id;
  $scope.id = parseInt(id);

  // get data
  $http({
    method: "get",
    url: "http://nas-arr.ddns.net/ProductApi/ProductList"
  }).then(
    function(response) {
      
      $scope.data = response.data;
      $scope.products = $scope.data;
      $scope.products.forEach(function(product) {
        if ($scope.id === product.ID) {
          $scope.product = product;
        }
      }, this);
    },
    function(error) {
      console.log(error, "can not get data.");
    }
  );

  $scope.actionExport = function(type) {
    switch (type) {
      case "csv":
        angular.element(".buttons-csv").click();
        break;
      case "excel":
        angular.element(".buttons-excel").click();
        break;
      case "pdf":
        angular.element(".buttons-pdf").click();
        break;
      case "print":
        angular.element(".buttons-print").click();
        break;
    }
  };

  $scope.showBtn = function(type) {
      switch(type) {
          case 'generateQuotation':
              return $state.includes('app.orders.order.product')
                  || $state.includes('app.orders.order.info')
                  || $state.includes('app.orders.order.log');
              break;
          case 'deliver':
              return $state.includes('app.orders.order.product');
              break;
          default:
              return false;
              break;
      }
  }

});