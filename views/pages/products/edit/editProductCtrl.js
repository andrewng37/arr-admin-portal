angular
.module('app')
.controller('ProductsEditCtrl', function ($scope, $stateParams, $http) {
    var id = $stateParams.id;
    $scope.id = parseInt(id);

    // get data
    $http({
      method: "get",
      url: "http://nas-arr.ddns.net/ProductApi/ProductList"
    }).then(function(response) {
        $scope.data = response.data;
        $scope.products = $scope.data;
        
        $scope.products.forEach(function(product) {
          
          if ($scope.id === product.ID) {
            $scope.product = product;
          }
        }, this);
      }, function(error) {
        console.log(error, "can not get data.");
      });

});