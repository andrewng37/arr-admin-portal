//orders.js
angular.module("app").controller("ProductsListCtrl", ProductsListCtrl);

ProductsListCtrl.$inject = ["$scope", "$http", "$state"];

function ProductsListCtrl($scope, $http, $state) {
  $http({
    method: "get",
    url: "http://nas-arr.ddns.net/ProductApi/ProductList"
  }).then(
    function(response) {
      $scope.data = response.data;
      $scope.products = $scope.data;
      $scope.products.forEach(function(product) {
        if (product.Created_On_Date) {
          $scope.ex =  product.Created_On_Date.slice(6, 19);
          product.Created_On_Date = new Date(parseInt($scope.ex));
        }
      }, this);
    },
    function(error) {
      console.log(error, "can not get data.");
    }
  );

  $scope.toggleAll = () => {
    var toggleStatus = $scope.isAllSelected;
    $scope.products.forEach(function(itm) {
      itm.selected = toggleStatus;
    }, this);
  };

  $scope.optionToggled = () => {
    $scope.isAllSelected = $scope.products.every(function(itm) {
      return itm.selected;
    });
  };

  
  $scope.goTo = function(pr1, pr2) {
    $state.go("app.orders.order.edit", { id: 1, id_item: 1 });
  };

  angular.element(document).ready(function() {
    angular.element("#lgt-tbl-products-list").DataTable({
      dom:
        '<"#lgt-export-btns.lgt-hidden"B>r<"#lgt-datatable-length"l><"#lgt-datatable-paginate"p>t',
      bDestroy: true,
      buttons: ["csv", "excel", "pdf", "print"],
      pagingType: "full_numbers",
      language: {
        lengthMenu: " _MENU_ ",
        paginate: {
          first: '<i class="fa fa-step-backward">',
          previous: '<i class="fa fa-caret-left">',
          next: '<i class="fa fa-caret-right">',
          last: '<i class="fa fa-step-forward">'
        }
      }
    });
  });
}
