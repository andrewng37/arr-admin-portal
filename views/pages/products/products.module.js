(function() {
    angular
      .module("app.pages.products", [
        "ui.router",
      ])
      .config(routeConfig);
  
    function routeConfig($stateProvider) {
      $stateProvider
        .state("app.products", {
          url: "/products",
          abstract: true,
          templateUrl: "views/pages/products/products_template.html",
          ncyBreadcrumb: {
            label: "Product"
          }
        })
        .state("app.products.list", {
          url: "/list",
          controller: "ProductsListCtrl",
          templateUrl: "views/pages/products/list.html",
          ncyBreadcrumb: {
            label: "Products Management"
          },
          params: {
            subtitle:
              ""
          },
          resolve: {
            loadMyCtrl: [
              "$ocLazyLoad",
              function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  files: [
                    "vendors/js/jquery.dataTables.min.js",
                    "dist/datatables_extensions.min.js"
                  ]
                });
              }
            ]
          }
        })
        .state("app.products.edit", {
          url: "/:id",
          controller: "ProductsEditCtrl",
          templateUrl: "views/pages/products/edit/edit_product.html",
          ncyBreadcrumb: {
            label: "Edit Products Detail"
          },
          params: {
            params: { id: null },
          },
          resolve: {
            loadMyCtrl: [
              "$ocLazyLoad",
              function($ocLazyLoad) {
                return $ocLazyLoad.load({
                  files: [
                    "vendors/js/jquery.dataTables.min.js",
                    "dist/datatables_extensions.min.js"
                  ]
                });
              }
            ]
          }
        });
        
    }
  })();