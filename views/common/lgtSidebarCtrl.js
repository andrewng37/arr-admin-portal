angular.module("app").controller("lgtSidebarCtrl", function($scope) {
  $scope.menuItems = [
    {
      title: "Orders",
      href: "app.orders",
      icon: "fa fa-shopping-cart",
      subMenu: [
        {
          title: "Orders",
          href: "app.orders.list",
          blank: false
        },
        {
          title: "Delivery",
          href: "app.orders.delivery.list",
          blank: false
        }
      ]
    },
    {
      title: "Catalog",
      href: "app.products",
      icon: "fa fa-book",
      subMenu: [
        {
          title: "Products",
          href: "app.products.list",
          blank: false
        }
      ]
    },
    // {
    //     title: 'Public Content',
    //     href: 'app.products',
    //     icon: 'fa fa-user',
    //     subMenu: [
    //         {
    //             title: 'Sample Page',
    //             href: 'app.sample',
    //             blank: false
    //         }
    //     ]
    // },
    {
      title: "Promotions",
      href: "app.promotions",
      icon: "fa fa-tags",
      subMenu: [
        {
          title: "Management",
          href: "app.promotions.list",
          blank: false
        }
      ]
    },
    {
      title: "Configuration",
      href: "app.configuration",
      icon: "fa fa-cubes",
      subMenu: [
          {
            title: "Internal Notification",
            href: "app.configuration.notification.list",
            blank: false
          },
        // {
        //   title: "Access Control",
        //   href: "",
        //   blank: false
				// },
				{
           title: "Kiosk",
           href: "app.configuration.kiosk",
           blank: false
				},
				// {
        //   title: "Order",
        //   href: "",
        //   blank: false
				// },
				// {
        //   title: "Delivery",
        //   href: "",
        //   blank: false
        // },
				// {
        //   title: "AA Fee",
        //   href: "",
        //   blank: false
        // },
				// {
        //   title: "Tax",
        //   href: "",
        //   blank: false
        // }
      ]
    }
    // {
    //     title: 'Configuration',
    //     href: 'app.products',
    //     icon: 'fa fa-cogs',
    //     subMenu: [
    //         {
    //             title: 'Sample Page',
    //             href: 'app.sample',
    //             blank: false
    //         }
    //     ]
    // },
    // {
    //     title: 'System Settings',
    //     href: 'app.products',
    //     icon: 'fa fa-server',
    //     subMenu: [
    //         {
    //             title: 'Sample Page',
    //             href: 'app.sample',
    //             blank: false
    //         }
    //     ]
    // },
  ];
});
