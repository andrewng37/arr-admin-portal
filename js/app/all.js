var GENERATE_ITEMS_LOCAL_STORAGE_KEY = 'GENERATE';
var TAX_INVOICE_INFO = 'GENINVINFO';
var TENANT_CODE = 'TENANT';
var PREV_TENANT_CODE = 'PREV_TENANT';
var TENANT_COUNT = 'TENANT_COUNT';
var FAQ_DRAFT_STORAGE_KEY = 'ARRFAQDRAFT';
// controller.js

angular.module("app").config(function () {
    angular.lowercase = angular.$$lowercase;
});

angular.module("app").controller("MainApp", function ($scope, $rootScope, $stateParams, $http, $state, $window, ApiService, $timeout) {

    $scope.session = true;

    ApiService.CheckAdminSession().then(
        function (r) {
            var data = r.data;

            if (data.Status == 'OK') {
                $scope.isSuperAdmin = data.ViewPersonalInfo;

                if (data.AuthUrl) {
                    $scope.session = false;
                    $timeout(function () {
                        $window.location = data.AuthUrl;
                    }, 100);

                } else {
                    $scope.session = true;
                }

                if (data.Groups) {
                    $scope.group = data.Groups[1];
                }

                if (data.UserName) {
                    $rootScope.username = data.UserName;
                }


                //if(sessionStorage[TENANT_COUNT] > 1){

                //}

            } else {
                $scope.errorMessage = data.ErrorMessage;
                $window.alert($scope.errorMessage);
            }
        },
        function (error) {
            //console.log(error);
        }
    );

    $scope.logoff = function () {
        sessionStorage.clear();
        $window.location = "/ARR/PublicApi/SessionApi/AdminLogOff";
    }

    var cur_tenantCode = sessionStorage[TENANT_CODE] ? JSON.parse(sessionStorage[TENANT_CODE]) : "";

    getTenant(cur_tenantCode);

    function getTenant(data) {
        ApiService.getaccess(data).then(
            function (r) {
                $scope.tenants = r.data.Tenants;
                $scope.selectedName = $scope.tenants[0].Description;
                ApiService.CheckAdminSession().then(
                    function (r) {
                        var data = r.data;

                        if (data.Status == 'OK') {
                            $scope.isSuperAdmin = data.ViewPersonalInfo;

                            if (data.AuthUrl) {
                                $scope.session = false;
                                $timeout(function () {
                                    $window.location = data.AuthUrl;
                                }, 100);

                            } else {
                                $scope.session = true;
                            }

                            if (data.Groups) {
                                $scope.group = data.Groups[1];
                            }

                            if (data.UserName) {
                                $rootScope.username = data.UserName;
                            }


                            //if(sessionStorage[TENANT_COUNT] > 1){

                            //}

                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {
                        //console.log(error);
                    }
                );

            },
            function (error) {
                console.log(error);
            }
        );
    }

});


angular
    .module("app")
    .controller("OrdersCtrl", function ($scope, $stateParams, $http, $state, ApiService) {
        var id = $stateParams.id;
        $scope.code2 = $stateParams.code2;
        $scope.id = id;

        $scope.actionExport = function (type) {
            switch (type) {
                case "csv":
                    angular.element(".buttons-csv").click();
                    break;
                case "excel":
                    angular.element(".buttons-excel").click();
                    break;
                case "pdf":
                    angular.element(".buttons-pdf").click();
                    break;
                case "print":
                    angular.element(".buttons-print").click();
                    break;
            }
        };


        $scope.productGenerateBtn = true;

        $scope.showQuotationSelection = function (InvOrDeliver) {

            if (InvOrDeliver == "invoice") {
                $scope.toggleQuotation = true;
                if ($scope.generateInvoice == true) {
                    $scope.generateInvoice = false;
                    $scope.productGenerateBtn = true;
                } else {
                    $scope.generateInvoice = true;
                    $scope.productGenerateBtn = false;

                }
            } else if (InvOrDeliver == "deliver") {
                $scope.toggleDelivery = true;
                if ($scope.generateDelivery == true) {
                    $scope.generateDelivery = false;
                    $scope.productGenerateBtn = true;
                } else {
                    $scope.generateDelivery = true;
                    $scope.productGenerateBtn = false;

                }
            }
        }

        $scope.cancelGenerate = function () {
            $scope.toggleDelivery = false;
            $scope.toggleQuotation = false;
            $scope.toggleDelivery = false;
            $scope.generateDelivery = false;
            $scope.generateInvoice = false;
            $scope.productGenerateBtn = true;
        }

        $scope.showBtn = function (type) {
            switch (type) {
                case 'generateQuotation':
                    return $state.includes('app.orders.order.product');
                    break;
                case 'deliver':
                    return $state.includes('app.orders.order.product');
                    break;
                default:
                    return false;
                    break;
            }
        }

    })
    .directive('lgtSearchFilter', lgtSearchFilter);

function lgtSearchFilter() {
    return {
        templateUrl: 'views/components/lgt-search-filter.html'
    }
}


//orders.js
angular.module("app").controller("OrdersListCtrl", ["$scope", "ApiService", "$window", "$state", "anchorSmoothScroll", "$timeout",
    function OrdersListCtrl($scope, ApiService, $window, $state, anchorSmoothScroll, $timeout) {
        var arr = [];

        //OrdersListCtrl.$inject = ["$scope", "$http", "$state"];

        //ApiService.GetOrder().then(
        //    function (response) {
        //        var data = response.data;
        //        if (data.Status == 'OK') {
        //            console.log(data);
        //            $scope.ordersCount = response.data.Orders;
        //            $scope.ordersLength = Math.round(parseInt($scope.ordersCount.length) / 7);
        //        } else {
        //            $scope.errorMessage = data.ErrorMessage;
        //            console.log($scope.errorMessage);
        //        }
        //    }, function (error) {
        //        console.log(error, "can not get data.");
        //    }
        //);

        $scope.formFilter = {
            PageSize: 7,
            PageNumber: 1,

        }
        $scope.pageNumber = 1;
        $scope.minusCount = function () {
            angular.element('.collapse').collapse('hide');
            var num = parseInt(angular.element('#quantity').val()) - 1;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: num,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getOrder($scope.formFilter);
        }

        $scope.addCount = function () {
            angular.element('.collapse').collapse('hide');
            var num = parseInt(angular.element('#quantity').val()) + 1;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: num,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getOrder($scope.formFilter);
        }

        $scope.pageNumberChange = function () {
            angular.element('.collapse').collapse('hide');
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: $scope.pageNumber,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }

            getOrder($scope.formFilter);
        }

        $scope.firstPage = function () {
            angular.element('.collapse').collapse('hide');
            angular.element('#quantity').val(1);
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: 1,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getOrder($scope.formFilter);
        }

        $scope.lastPage = function () {
            angular.element('.collapse').collapse('hide');
            angular.element('#quantity').val($scope.ordersLength);
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: $scope.ordersLength,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getOrder($scope.formFilter);
        }

        getOrder($scope.formFilter);

        function getOrder(data) {
            ApiService.getworkorder(data).then(
                function (response) {
                    var data = response.data;
                    $scope.orders = response.data.WorkOrders;
                    $scope.ordersLength = response.data.TotalPages;
                    $scope.filtering = false;

                });
            //     ApiService.GetOrder(data).then(
            //         function(response) {
            //             var data = response.data;

            //             if (data.Status == 'OK') {
            //                 $scope.orders = response.data.Orders;
            //                 $scope.ordersLength = response.data.TotalPages;
            //                 $scope.groupItems = [];
            //                 var count = 0;
            //                 $scope.orderItemsByGroupCode = {};

            //                 $.each($scope.orders, function(index1, order) {
            //                     $.each(order.Items, function(index2, item) {
            //                         if (!$scope.orderItemsByGroupCode[item.GroupCode]) {
            //                             $scope.orderItemsByGroupCode[item.GroupCode] = {
            //                                 OrderId: order.OrderId,
            //                                 ItemGroupCode: item.GroupCode,
            //                                 categoryCode: item.CategoryCode,
            //                                 OrderItemId: item.OrderItemId,
            //                                 Category: item.Description,
            //                                 RequestorName: order.Requestor.Name,
            //                                 LastModifiedDate: item.LastModifiedDate,
            //                                 CreatedDate: order.OrderCreatedDate,
            //                                 RequireAction: false,
            //                                 Items: []
            //                             };
            //                             count++;
            //                         }

            //                         if ($scope.orderItemsByGroupCode[item.GroupCode].LastModifiedDate > item.LastModifiedDate) {
            //                             $scope.orderItemsByGroupCode[item.GroupCode].LastModifiedDate = item.LastModifiedDate;
            //                         }

            //                         item.RequireAction = item.StatusCode == 'PV' || item.StatusCode == 'PR' || item.StatusCode == 'PQ' || item.StatusCode == 'PS';
            //                         $scope.orderItemsByGroupCode[item.GroupCode].RequireAction = $scope.orderItemsByGroupCode[item.GroupCode].RequireAction || item.RequireAction;
            //                         $scope.orderItemsByGroupCode[item.GroupCode].Items.push(item);
            //                     });
            //                 });


            //                 $scope.count = count;
            //                 $scope.filtering = false;
            //             } else {
            //                 $scope.errorMessage = data.ErrorMessage;
            //                 console.log($scope.errorMessage);
            //             }
            //         },
            //         function(error) {
            //             console.log(error, "can not get data.");
            //         }
            //     );

        }

        $scope.getOrderItemsList = function (GroupCode, index, id) {

            var $row = angular.element("#item-" + index + "-" + id);
            $row.html("");
            var $boxes = '';
            $scope.setGroupCode = {
                GroupCode: GroupCode
            }
            ApiService.GetWorkOrderItem($scope.setGroupCode).then(
                function (response) {
                    var itemData = response.data.WorkOrderItems;
                    itemData.forEach(function (item) {

                        var InternalActionRequired = "";
                        if (item.InternalActionRequired) {
                            InternalActionRequired = '<div style="margin: auto;" class="icon-alert text-center">!</div>';
                        } else {
                            InternalActionRequired = '';
                        }
                        $boxes = '<tr class="body-table-orders-items accordion-toggle"><td>' + InternalActionRequired + '</td><td>' + item.ItemCode + '</td><td>' + item.SourceNo + '</td><td style="width:30%">' + item.SourceTitle + '</td><td class="text-center">' + item.Format + '</td><td>' + item.InternalDescription + '</td><td class="text-center">' + item.PaymentStatus + '</td><td>' + item.LastModifiedUserId + '</td></tr>';
                        $row.append($boxes);
                    });
                });
        }

        $scope.filterSubmit = function () {
            angular.element('.collapse').collapse('hide');
            $scope.filtering = true;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: 1,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }

            $scope.ordersCount = $scope.count;
            // $scope.ordersLength = Math.round(parseInt($scope.ordersCount) / 7);
            // anchorSmoothScroll.scrollTo('lgt-tbl-orders-list');
            getOrder($scope.formFilter);
        }

        //$scope.selection = [];
        ////$scope.selectedDesc = [];
        //$scope.toggleSelection = function toggleSelection(status) {

        //    var idx = $scope.selection.indexOf(status);

        //    if (idx > -1) {
        //        $scope.selection.splice(idx, 1);
        //       // $scope.selectedDesc.splice(idx, 1);
        //    }
        //    else {
        //        $scope.selection.push(status);
        //       // $scope.selectedDesc.push(desc);
        //    }
        //};

        $scope.setShopCode = function (code) {
            $scope.gettingStatus = true;
            $scope.statusList = [];

            ApiService.GetCategory().then(
                function (response) {
                    var data = response.data;
                    if (data.Status == 'OK') {
                        $scope.compare_cate = response.data.category;
                        $scope.compare_cate.forEach(function (cate) {

                            if (cate.CategoryCode == code) {

                                $scope.getShopCode = cate.ShopCode;
                            }
                        })
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error);
                }
            );

            ApiService.GetOrderStatus().then(
                function (res) {
                    var data = res.data;
                    if (data.Status == 'OK') {
                        //$scope.rejectCode = [];
                        //$scope.allStatus = [];
                        $scope.status_compare = res.data.OrderStatus;
                        $scope.status_compare.forEach(function (status) {
                            if (status.ShopCode == $scope.getShopCode || !status.ShopCode) {
                                $scope.statusList.push(status);
                            }
                        })
                        $scope.gettingStatus = false;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );


        }


        $scope.filter = {
            route: ''
        }
        ApiService.GetCategory().then(
            function (response) {
                var data = response.data;
                if (data.Status == 'OK') {

                    $scope.categories = response.data.category;
                } else {
                    $scope.errorMessage = data.ErrorMessage;
                    console.log($scope.errorMessage);
                }
            },
            function (error) {
                console.log(error);
            }
        );

        //ApiService.GetOrderStatus().then(
        //    function (res) {
        //        var data = res.data;
        //        if (data.Status == 'OK') {
        //            //$scope.rejectCode = [];
        //            //$scope.allStatus = [];
        //            $scope.statusData = res.data.OrderStatus;


        //        } else {
        //            $scope.errorMessage = data.ErrorMessage;
        //            console.log($scope.errorMessage);
        //        }
        //    },
        //    function (error) {
        //        console.log(error, "can not get data.");
        //    }
        //);



        $scope.loading = false;
        // $scope.routeFilter = function() {
        //     $scope.loading = true;
        //     ApiService.GetOrder().then(
        //         function(response) {
        //             var data = response.data;
        //             if (data.Status == 'OK') {
        //                 $scope.loading = false;
        //                 $scope.orders = response.data.Orders;
        //                 var count = 0;
        //                 $scope.orderItemsByGroupCode = {};

        //                 $.each($scope.orders, function(index1, order) {
        //                     $.each(order.Items, function(index2, item) {
        //                         if ($scope.filter.route) {
        //                             if (item.CategoryRouteCode == $scope.filter.route) {
        //                                 if (!$scope.orderItemsByGroupCode[item.GroupCode]) {
        //                                     $scope.orderItemsByGroupCode[item.GroupCode] = {
        //                                         OrderId: order.OrderId,
        //                                         ItemGroupCode: item.GroupCode,
        //                                         categoryCode: item.CategoryCode,
        //                                         OrderItemId: item.OrderItemId,
        //                                         Category: item.Description,
        //                                         RequestorName: order.Requestor.Name,
        //                                         LastModifiedDate: item.LastModifiedDate,
        //                                         CreatedDate: order.OrderCreatedDate,
        //                                         RequireAction: false,
        //                                         Items: []

        //                                     };
        //                                     count++;
        //                                 }

        //                                 if ($scope.orderItemsByGroupCode[item.GroupCode].LastModifiedDate > item.LastModifiedDate) {
        //                                     $scope.orderItemsByGroupCode[item.GroupCode].LastModifiedDate = item.LastModifiedDate;
        //                                 }

        //                                 item.RequireAction = item.StatusCode == 'PV' || item.StatusCode == 'PR' || item.StatusCode == 'PQ' || item.StatusCode == 'PS';
        //                                 $scope.orderItemsByGroupCode[item.GroupCode].RequireAction = $scope.orderItemsByGroupCode[item.GroupCode].RequireAction || item.RequireAction;
        //                                 $scope.orderItemsByGroupCode[item.GroupCode].Items.push(item);
        //                             }

        //                         } else {
        //                             if (!$scope.orderItemsByGroupCode[item.GroupCode]) {
        //                                 $scope.orderItemsByGroupCode[item.GroupCode] = {
        //                                     OrderId: order.OrderId,
        //                                     ItemGroupCode: item.GroupCode,
        //                                     categoryCode: item.CategoryCode,
        //                                     OrderItemId: item.OrderItemId,
        //                                     Category: item.Description,
        //                                     RequestorName: order.Requestor.Name,
        //                                     LastModifiedDate: item.LastModifiedDate,
        //                                     CreatedDate: order.OrderCreatedDate,
        //                                     RequireAction: false,
        //                                     Items: []

        //                                 };
        //                                 count++;
        //                             }

        //                             if ($scope.orderItemsByGroupCode[item.GroupCode].LastModifiedDate > item.LastModifiedDate) {
        //                                 $scope.orderItemsByGroupCode[item.GroupCode].LastModifiedDate = item.LastModifiedDate;
        //                             }

        //                             item.RequireAction = item.StatusCode == 'PV' || item.StatusCode == 'PR' || item.StatusCode == 'PQ' || item.StatusCode == 'PS';
        //                             $scope.orderItemsByGroupCode[item.GroupCode].RequireAction = $scope.orderItemsByGroupCode[item.GroupCode].RequireAction || item.RequireAction;
        //                             $scope.orderItemsByGroupCode[item.GroupCode].Items.push(item);
        //                         }

        //                     });

        //                 });
        //                 $scope.count = count;
        //             } else {
        //                 $scope.errorMessage = data.ErrorMessage;
        //                 console.log($scope.errorMessage);
        //             }
        //         },
        //         function(error) {
        //             console.log(error, "can not get data.");
        //         }
        //     );
        // }

        //$scope.toggleAll = () => {
        //    var toggleStatus = $scope.isAllSelected;
        //    $scope.orders.forEach(function (itm) {
        //        console.log(itm);
        //        itm.selected = toggleStatus;
        //    }, this);
        //};

        //$scope.optionToggled = () => {
        //    $scope.isAllSelected = $scope.orders.every(function (itm) {
        //        return itm.selected;
        //    });
        //};

    }
]);

angular
    .module("app")
    .controller("ProductsCtrl", function ($scope, $stateParams, $http, $state) {
        var id = $stateParams.id;
        $scope.id = parseInt(id);

        $scope.actionExport = function (type) {
            switch (type) {
                case "csv":
                    angular.element(".buttons-csv").click();
                    break;
                case "excel":
                    angular.element(".buttons-excel").click();
                    break;
                case "pdf":
                    angular.element(".buttons-pdf").click();
                    break;
                case "print":
                    angular.element(".buttons-print").click();
                    break;
            }
        };

        $scope.showBtn = function (type) {
            switch (type) {
                case 'generateQuotation':
                    return $state.includes('app.orders.order.product') ||
                        $state.includes('app.orders.order.info') ||
                        $state.includes('app.orders.order.log');
                    break;
                case 'deliver':
                    return $state.includes('app.orders.order.product');
                    break;
                default:
                    return false;
                    break;
            }
        }

    });
//orders.js
angular.module("app").controller("ProductsListCtrl", ProductsListCtrl);

ProductsListCtrl.$inject = ["$scope", "$http", "$state", "ApiService"];

function ProductsListCtrl($scope, $http, $state, ApiService) {

    ApiService.GetPriceRule().then(
        function (response) {
            var data = response.data;
            if (data.Status == 'OK') {
                $scope.rules = data.PriceRules;
                $scope.viewby = 10;
                $scope.totalItems = $scope.rules.length;
                $scope.currentPage = 1;
                $scope.itemsPerPage = $scope.viewby;
                $scope.maxSize = 5;
            } else {
                $scope.errorMessage = data.ErrorMessage;
                alert($scope.errorMessage);
            }
        },
        function (error) {
            console.log(error);
        }
    );

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function () {
        //console.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.setItemsPerPage = function (num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1; //reset to first page
    }

    //$scope.toggleAll = () => {
    //    var toggleStatus = $scope.isAllSelected;
    //    $scope.products.forEach(function (itm) {
    //        itm.selected = toggleStatus;
    //    }, this);
    //};

    //$scope.optionToggled = () => {
    //    $scope.isAllSelected = $scope.products.every(function (itm) {
    //        return itm.selected;
    //    });
    //};

    //$scope.goTo = function (pr1, pr2) {
    //    $state.go("app.orders.order.edit");
    //};

    //angular.element(document).ready(function () {
    //    angular.element("#lgt-tbl-products-list").DataTable({
    //        dom:
    //            '<"#lgt-export-btns.lgt-hidden"B>r<"#lgt-datatable-length"l><"#lgt-datatable-paginate"p>t',
    //        bDestroy: true,
    //        buttons: ["csv", "excel", "pdf", "print"],
    //        pagingType: "full_numbers",
    //        language: {
    //            lengthMenu: " _MENU_ ",
    //            paginate: {
    //                first: '<i class="fa fa-step-backward">',
    //                previous: '<i class="fa fa-caret-left">',
    //                next: '<i class="fa fa-caret-right">',
    //                last: '<i class="fa fa-step-forward">'
    //            }
    //        }
    //    });
    //});
}
//orders.js
angular.module("app").controller("PromotionsAddCtrl", PromotionsAddCtrl)
    .directive('lgtPromotionsAddForm', lgtPromotionsAddForm);

PromotionsAddCtrl.$inject = ["$scope", "$http", "$state"];

function PromotionsAddCtrl($scope, $http, $state) {
    $state.current.data = {
        back: true,
        backto: "app.promotions.list"
    };

    $scope.showBtnDelete = false;

    $scope.promotionCode = {
        promoCode: null,
        description: null,
        discountValue: null,
        discountType: null,
        validFrom: null,
        validUntil: null,
        status: null,
        createdDate: '7 Aug 2018',
        createdBy: 'John',
        modifiedDate: '7 Aug 2018',
        modifiedBy: 'John'
    };

    angular.element(document).ready(function () {
        angular.element('#lgt-promotions-form').submit(function (e) {
            e.preventDefault();
        });
    });
}

function lgtPromotionsAddForm() {
    return {
        templateUrl: 'views/pages/promotions/promotions_form.html'
    }
}

angular
    .module("app")
    .controller("PromotionsCtrl", function ($scope, $stateParams, $http, $state) {
        var id = $stateParams.id;
        $scope.id = parseInt(id);

        $scope.actionExport = function (type) {
            switch (type) {
                case "csv":
                    angular.element(".buttons-csv").click();
                    break;
                case "excel":
                    angular.element(".buttons-excel").click();
                    break;
                case "pdf":
                    angular.element(".buttons-pdf").click();
                    break;
                case "print":
                    angular.element(".buttons-print").click();
                    break;
            }
        };

        $scope.showBtn = function (type) {
            switch (type) {
                case 'export':
                    return $state.includes('app.promotions.list');
                    break;
                default:
                    return false;
                    break;
            }
        }

    });
//orders.js
angular.module("app").controller("PromotionsListCtrl", PromotionsListCtrl);

PromotionsListCtrl.$inject = ["$scope", "$http", "$state"];

function PromotionsListCtrl($scope, $http, $state) {
    $http({
        method: "get",
        url: "js/data_promotions.json"
    }).then(
        function (response) {
            $scope.labels = response.data.labels;
            $scope.items = response.data.items;
        },
        function (error) {
            console.log(error, "There is something wrong with getting data.");
        }
    );

    $scope.toggleAll = function () {
        var toggleStatus = $scope.isAllSelected;
        $scope.items.forEach(function (item) {
            item.selected = toggleStatus;
        }, this);
    };

    $scope.optionToggled = function () {
        $scope.isAllSelected = $scope.items.every(function (item) {
            return item.selected;
        });
    };

    angular.element(document).ready(function () {
        $scope.table = angular.element("#lgt-tbl-promotions-list").DataTable({
            dom: '<"#lgt-export-btns.lgt-hidden"B>r<"#lgt-datatable-length"l><"#lgt-datatable-paginate"p>t',
            bDestroy: true,
            buttons: ["csv", "excel", "pdf", "print"],
            pagingType: "full_numbers",
            language: {
                lengthMenu: " _MENU_ ",
                paginate: {
                    first: '<i class="fa fa-step-backward">',
                    previous: '<i class="fa fa-caret-left">',
                    next: '<i class="fa fa-caret-right">',
                    last: '<i class="fa fa-step-forward">'
                }
            }
        });
    });
}

angular.module("app")
    .controller("PromotionsModifyCtrl", PromotionsModifyCtrl)
    .directive('lgtPromotionsModifyForm', lgtPromotionsModifyForm);

PromotionsModifyCtrl.$inject = ["$scope", "$http", "$state", "$stateParams"];

function PromotionsModifyCtrl($scope, $http, $state, $stateParams) {
    $scope.id = $stateParams.id;

    $state.current.data = {
        back: true,
        backto: "app.promotions.list"
    };

    $scope.showBtnDelete = true;

    $http.get('js/data_promotions.json').then(function (res) {
        var items = res.data.items;
        items.forEach(function (value, key) {
            if (value.id == $scope.id) {
                $scope.promotionCode = value;
            }
        })
    }, function (err) {
        console.log('ERROR');
        console.log(err);
    });


    angular.element(document).ready(function () {
        angular.element('#lgt-promotions-form').submit(function (e) {
            e.preventDefault();
        });
    });

}

function lgtPromotionsModifyForm() {
    return {
        templateUrl: 'views/pages/promotions/promotions_form.html'
    }
}

angular
    .module("app")
    .controller("KioskCtrl", function (
        $state,
        $scope,
        $stateParams,
        $http
    ) {
        // get data
        $http({
            method: "get",
            url: "js/data.json"
        }).then(
            function (response) {

                $scope.data = response.data;
                $scope.notifications = $scope.data.notifications;
                console.log($scope.notifications);
            },
            function (error) {
                console.log(error, "can not get data.");
            }
        );

        $state.current.params.subtitle = 'Last edited by: Kelvin Tan, 18/08/2018, 12.03PM';
    });




angular
    .module("app")
    .controller("OrderCtrl", function ($scope, $stateParams, $http, ApiService) {
        var id = $stateParams.id;
        // get data
        var code2 = $stateParams.code2;
        $scope.GroupCodeTitle = code2;
        ApiService.GetOrder({
            "RequestId": $stateParams.id
        }).then(
            function (response) {
                $scope.order = response.data.Orders[0];
            },
            function (error) {
                console.log(error, "can not get data.");
            }
        );

        $scope.actionExport = function (type) {
            switch (type) {
                case "csv":
                    angular.element(".buttons-csv").click();
                    break;
                case "excel":
                    angular.element(".buttons-excel").click();
                    break;
                case "pdf":
                    angular.element(".buttons-pdf").click();
                    break;
                case "print":
                    angular.element(".buttons-print").click();
                    break;
            }
        };
    });

angular
    .module('app')
    .controller('ProductsEditCtrl', function ($scope, $stateParams, $http) {
        var id = $stateParams.id;
        $scope.id = parseInt(id);


    });

(function () {
    angular.module("app.pages.products.edit", ["ui.router"]);
})();

(function () {
    angular
        .module("app")
        .controller("NotiEditCtrl", function (
            $scope,
            $stateParams,
            $http,
            $state,
            $timeout
        ) {
            var id = $stateParams.id;

            $scope.id = parseInt(id);

            tinymce.init({
                selector: '#mytextarea'
            });

            $http.get("js/data.json").then(
                function (response) {
                    $scope.data = response.data;
                    $scope.notifications = $scope.data.notifications;
                    $scope.notifications.forEach(function (notification) {
                        if ($scope.id === notification.id) {
                            $scope.notification = notification;
                        }
                    }, this);
                },
                function (error) {
                    console.log(error);
                }
            );



        });
})();

(function () {
    angular.module("app.pages.configuration.notification.edit", ["ui.router"]);
})();

angular
    .module("app")
    .controller("NotificationCtrl", function (
        $state,
        $scope,
        $stateParams,
        $http
    ) {
        // get data
        $http({
            method: "get",
            url: "js/data.json"
        }).then(
            function (response) {
                $scope.data = response.data;
                $scope.notifications = $scope.data.notifications;
            },
            function (error) {
                console.log(error, "can not get data.");
            }
        );


    });

(function () {
    angular.module("app.pages.configuration.notification.list", ["ui.router"]);
})();

(function () {
    angular
        .module("app")
        .controller("DeliveryEditCtr", function ($scope, $stateParams, $http, $state) {

            var id = $stateParams.id;

            $scope.id = parseInt(id);

            // var id_item = $stateParams.id_item;
            // $scope.id_item = parseInt(id_item);

            $http.get("js/data.json").then(
                function (response) {
                    $scope.data = response.data;
                    $scope.deliverys = $scope.data.deliverys;
                    $scope.deliverys.forEach(function (delivery) {
                        if ($scope.id === delivery.id) {
                            $scope.delivery = delivery;
                        }
                    }, this);
                    // $scope.order["items"].forEach(function(item) {
                    //   if ($scope.id_item === item.id) {
                    //     $scope.item = item;
                    //   }
                    // }, this);
                },
                function (error) {
                    console.log(error);
                }
            );

        });
})();

(function () {
    angular.module("app.pages.orders.delivery.edit", ["ui.router"]);
})();

angular
    .module("app")
    .controller("DeliveryCtrl", function ($scope, $stateParams, $http, $state, ApiService, $window) {
        $scope.deliverys = [];

        //$scope.setPage = function (pageNo) {
        //    $scope.currentPage = pageNo;
        //};

        //$scope.pageChanged = function () {
        //    //console.log('Page changed to: ' + $scope.currentPage);
        //};

        //$scope.setItemsPerPage = function (num) {
        //    $scope.itemsPerPage = num;
        //    $scope.currentPage = 1; //reset to first page
        //}

        $scope.formFilter = {
            PageSize: 7,
            PageNumber: 1
        }
        $scope.pageNumber = 1;

        $scope.filterSubmit = function () {

            $scope.filtering = true;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: 1,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }

            $scope.ordersCount = $scope.count;
            //$scope.ordersLength = Math.round(parseInt($scope.ordersCount) / 7);

            getDelivery($scope.formFilter);
        }

        $scope.addCount = function () {
            var num = parseInt(angular.element('#quantity').val()) + 1;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: num,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getDelivery($scope.formFilter);
        }

        $scope.minusCount = function () {
            var num = parseInt(angular.element('#quantity').val()) - 1;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: num,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getDelivery($scope.formFilter);
        }

        $scope.pageNumberChange = function () {

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: $scope.pageNumber,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }

            getDelivery($scope.formFilter);
        }

        $scope.firstPage = function () {
            angular.element('#quantity').val(1);
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: 1,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getDelivery($scope.formFilter);
        }

        $scope.lastPage = function () {
            angular.element('#quantity').val($scope.ordersLength);
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: $scope.ordersLength,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getDelivery($scope.formFilter);
        }

        $scope.setShopCode = function (code) {
            $scope.gettingStatus = true;
            $scope.statusList = [];

            ApiService.GetCategory().then(
                function (response) {
                    var data = response.data;
                    if (data.Status == 'OK') {
                        $scope.compare_cate = response.data.category;
                        $scope.compare_cate.forEach(function (cate) {

                            if (cate.CategoryCode == code) {

                                $scope.getShopCode = cate.ShopCode;
                            }
                        })
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error);
                }
            );

            ApiService.GetOrderStatus().then(
                function (res) {
                    var data = res.data;
                    if (data.Status == 'OK') {
                        //$scope.rejectCode = [];
                        //$scope.allStatus = [];
                        $scope.status_compare = res.data.OrderStatus;
                        $scope.status_compare.forEach(function (status) {
                            if (status.ShopCode == $scope.getShopCode || !status.ShopCode) {
                                $scope.statusList.push(status);
                            }
                        })
                        $scope.gettingStatus = false;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );


        }


        $scope.filter = {
            route: ''
        }
        ApiService.GetCategory().then(
            function (response) {
                var data = response.data;
                if (data.Status == 'OK') {

                    $scope.categories = response.data.category;
                } else {
                    $scope.errorMessage = data.ErrorMessage;
                    console.log($scope.errorMessage);
                }
            },
            function (error) {
                console.log(error);
            }
        );

        getDelivery($scope.formFilter);

        function getDelivery(filterData) {
            ApiService.GetDelivery(filterData).then(
                function (response) {
                    var data = response.data;
                    if (data.Status == 'OK') {

                        $scope.deliverys = response.data.RequestDelv;
                        $scope.count = $scope.deliverys.length;
                        $scope.ordersLength = response.data.TotalPages
                        //$scope.viewby = 10;
                        //$scope.totalItems = $scope.deliverys.length;
                        //$scope.currentPage = 1;
                        //$scope.itemsPerPage = $scope.viewby;
                        //$scope.maxSize = 5;
                        $scope.filtering = false;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                    // $scope.count = count;
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );
        }

        // $scope.toggleAll = () => {
        //     var toggleStatus = $scope.isAllSelected;
        //     $scope.deliverys.forEach(function(itm) {
        //         itm.selected = toggleStatus;
        //     }, this);
        // };

        // $scope.optionToggled = item => {

        //     $scope.isAllSelected = $scope.deliverys.every(function(itm) {
        //         return itm.selected;
        //     });
        // };

        $scope.generatedeliver = function (OrderId, GroupCode) {
            $state.go("app.orders.order.deliver", {
                'id': OrderId,
                'code1': GroupCode.charAt(0),
                'code2': GroupCode
            });
        }

        $scope.generateDelivery = function (OrderId, objId, GroupCode, devId) {

            $scope.itemss = {
                requestcode: OrderId,
                items: [objId],
                from: "deliveryList",
                deliverId: devId
            };

            sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY] = JSON.stringify($scope.itemss);
            $scope.generatedeliver(OrderId, GroupCode);
        }
    });

(function () {
    angular.module("app.pages.orders.delivery.list", ["ui.router"]);
})();

(function () {
    angular.module("app.pages.orders.order.deliver", [
        "ui.router",
        "ngAnimate",
        "ngSanitize",
        // "ui.bootstrap"
    ]);
})();

(function () {
    angular
        .module("app")
        .controller("deliverCtrl", function ($scope, $stateParams, $state, $filter, $timeout, ApiService, $window) {
            var statusCode = $stateParams.code1;
            $scope.getitems = [];
            $scope.itemslist = [];
            //$scope.itemslist = $stateParams.obj.items;
            //var getID = $scope.itemslist[0].Order_Item_ID;

            $scope.todayDate = new Date();
            $scope.curr_date = new Date();
            $scope.sixtyDays = new Date();
            $scope.sixtyDays = $scope.sixtyDays.setMonth($scope.sixtyDays.getMonth() + 2);
            $scope.getItemslist = JSON.parse(sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY]);
            $scope.comparedItems = $scope.getItemslist.items;

            if (statusCode == "C") {
                $scope.status_c = true;
                $scope.status_s = false;
                $scope.status_m = false;
                $scope.status_p = false;
            } else if (statusCode == "S") {
                $scope.status_c = false;
                $scope.status_s = true;
                $scope.status_m = false;
                $scope.status_p = false;
            } else if (statusCode == "P") {
                $scope.status_c = false;
                $scope.status_s = false;
                $scope.status_m = false;
                $scope.status_p = true;
            } else {
                $scope.status_c = false;
                $scope.status_s = false;
                $scope.status_m = true;
                $scope.status_p = false;
            }

            $scope.getLoc = $scope.getItemslist.from;
            $scope.requestid = {
                RequestId: $scope.getItemslist.requestcode
            }

            ApiService.GetOrder($scope.requestid).then(
                function (r) {
                    var data = r.data;
                    if (data.Status == 'OK') {
                        $scope.data = r.data.Orders;
                        $scope.data.forEach(function (item) {
                            $scope.rqstName = item.Requestor.Name;
                            $scope.rqstMobile = item.Requestor.Mobile;
                            $scope.rqstEmail = item.Requestor.Email;
                            $scope.getItems = item.Items;

                            $scope.getItems.forEach(function (each_item) {

                                $scope.orderItemId = each_item.OrderItemId;
                                $scope.StatusId = each_item.StatusId;
                                $scope.LastModifiedUserId = each_item.LastModifiedUserId;

                                if ($scope.comparedItems.indexOf($scope.orderItemId) > -1) {
                                    $scope.itemslist.push(each_item);
                                    $scope.unixOrderItemId = each_item.OrderItemId;
                                    $scope.unixInternalComments = each_item.InternalComments;
                                }
                            }, this);
                            $scope.OrderCode = item.OrderCode;

                            $scope.getDeliveryOrders = item.DeliveryOrders;

                            $scope.getDeliveryOrders.forEach(function (info) {

                                $scope.Infos = info.DeliveryInfos;
                                $scope.Infos.forEach(function (info_item) {
                                    if ($scope.comparedItems.indexOf(info_item.OrderItemId) > -1) {

                                        $scope.lockerNo = info_item.LockerNo;
                                        $scope.lockerPin = info_item.LockPin;
                                        $scope.staffName = info_item.StaffName;
                                        $scope.CollectionPoint = info_item.CollectionPoint;
                                        //$scope.DeliveryMtd = $scope.Infos[0].MethodCode;
                                        $scope.DeliveryMtd = info_item.MethodCode;
                                    }
                                });


                            }, this);

                        }, this);
                        //if (statusCode == "S" || statusCode == "M") {
                        $scope.email_template = {
                            OrderItemId: $scope.orderItemId,
                            StatusId: $scope.StatusId
                        }
                        ApiService.GetEmailTemplate($scope.email_template).then(
                            function (r) {
                                $scope.Body = r.data.Body;
                                $scope.Subject = r.data.Subject;

                                $scope.formData = {
                                    MethodCode: $scope.status_c || $scope.status_p ? $scope.DeliveryMtd ? $scope.DeliveryMtd : "DL-PUBLIC" : "EMAIL",
                                    htmlVariable: $scope.Body,
                                    tracking_no: "",
                                    locker_no: $scope.lockerNo,
                                    pin_no: $scope.lockerPin,
                                    StaffName: $scope.staffName,
                                    CollectionPoint: $scope.CollectionPoint,
                                    valid_date: $filter('date')($scope.curr_date, "yyyy-MM-dd"),
                                    valid_until: sixtyDaysAfter,
                                    InternalComments: $scope.unixInternalComments
                                }

                            },
                            function (error) {
                                console.log(error, "can not get data.");
                            }
                        );
                        //}

                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );

            ApiService.GetDeliveryMethods().then(
                function (r) {
                    var data = r.data;
                    if (data.Status == 'OK') {
                        $scope.DeliveryMethods = r.data.DeliveryMethods;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );

            var sixtyDaysAfter = new Date($scope.sixtyDays);

            $scope.deliveryConfirmModal = function () {
                $('#successModal').modal('show');
            }
            $scope.isFileUploaded = true;
            $(document).ready(function () {
                $("#fileDeliver").on("change", function () {

                    $scope.isFileUploaded = false;
                    $scope.uploadedFile = this.files[0];
                    $scope.hasUpload = true;
                    $scope.fileList = this.files;
                    $("#fileList").html(
                        '<table class="table table-striped table_deliver" ng-if="hasUpload"><thead><tr>' +
                        '<td>File Path</td></tr></thead>' +
                        '<tbody><tr><td>' + this.files[0].name + '</td></tr></tbody></table>'
                    );
                });
            });

            $(document).ready(function () {
                $("#generateMandS").on("click", function (e) {

                    $scope.loading = true;
                    var deliverdata = new FormData();
                    deliverdata.append("File", $scope.uploadedFile);
                    deliverdata.append("OrderCode", $scope.itemslist[0].OrderCode);

                    ApiService.Attachment(deliverdata).then(function (r) {
                        console.log(r.Status);
                        var attachmentData = r;
                        if (attachmentData.Status === "OK") {
                            //$scope.itemslist.forEach(function (item) {

                            $scope.deliverFormData = {
                                OrderItemIds: [$scope.itemslist[0].OrderItemId],
                                MethodCode: $scope.formData.MethodCode,
                                Body: $scope.formData.htmlVariable,
                                Subject: $scope.Subject,
                                LockerNo: $scope.formData.locker_no,
                                LockPin: $scope.formData.pin_no,
                                StaffName: $scope.formData.StaffName,
                                CollectionPoint: $scope.formData.CollectionPoint,
                                //TrackingNumber: $scope.formData.tracking_no,
                                //Delivery_Date: $scope.formData.valid_date,
                                //Delivery_ValidTill: $scope.formData.valid_until,
                                DueDate: $scope.formData.valid_until,
                                LastModifiedUserId: $scope.itemslist[0].LastModifiedUserId
                            }
                            ApiService.RegisterDelivery($scope.deliverFormData).then(
                                function (res) {
                                    var data = res.data;

                                    if (data.Status == 'OK') {
                                        $scope.loading = false;
                                        $('#successModal').modal('hide');
                                        sessionStorage.clear();
                                        $state.go("app.orders.order.product");
                                        $('.ifDeliverSuccess').show(100);
                                        $('.num_selected').text(0);
                                        $timeout(function () {
                                            $('.ifDeliverSuccess').hide(100);
                                        }, 2000);
                                    } else {
                                        $scope.errorMessage = data.ErrorMessage;
                                        $window.alert($scope.errorMessage);
                                    }
                                },
                                function (error) {
                                    console.log(error, "can not get data.");
                                });
                        } else {
                            $scope.errorMessage = attachmentData.ErrorMessage;
                            $window.alert($scope.errorMessage);
                            $scope.loading = false;
                            $('#successModal').modal('hide');
                        }
                    });

                    //}
                    //});
                });
            });

            $scope.UpdateDelivery = function () {
                $scope.deliverFormData = {
                    DelvOrderId: $scope.getItemslist.deliverId,
                    OrderItemId: $scope.unixOrderItemId,
                    MethodCode: $scope.formData.MethodCode,
                    LockerNo: $scope.formData.locker_no,
                    LockPin: $scope.formData.pin_no,
                    StaffName: $scope.formData.StaffName,
                    CollectionPoint: $scope.formData.CollectionPoint,
                    DueDate: $scope.formData.valid_until,
                    InternalComments: $scope.formData.InternalComments
                }

                ApiService.UpdateDelivery($scope.deliverFormData).then(
                    function (res) {
                        var data = res.data;
                        if (data.Status == 'OK') {

                            $('#successModal').modal('hide');
                            //sessionStorage.clear();
                            //$state.go("app.orders.delivery.list");
                            $state.transitionTo($state.current, $stateParams, {
                                reload: true,
                                inherit: false,
                                notify: true
                            });
                            $('.ifDeliverUpdateSuccess').show(100);
                            $timeout(function () {
                                $('.ifDeliverUpdateSuccess').hide(100);
                            }, 2000);
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {
                        console.log(error, "can not get data.");
                    });
            }

            $scope.deliveryFormSubmit = function ($event) {
                //$scope.itemslist.forEach(function (item) {
                $scope.deliverFormData = {
                    OrderItemIds: $scope.getItemslist.items,
                    MethodCode: $scope.formData.MethodCode,
                    Body: $scope.formData.htmlVariable,
                    Subject: $scope.Subject,
                    LockerNo: $scope.formData.locker_no,
                    LockPin: $scope.formData.pin_no,
                    StaffName: $scope.formData.StaffName,
                    CollectionPoint: $scope.formData.CollectionPoint,
                    //TrackingNo: $scope.formData.tracking_no,
                    //Delivery_Date: $scope.formData.valid_date,
                    //Delivery_ValidTill: $scope.formData.valid_until,
                    //LastModifiedUserId: item.LastModifiedUserId,
                    DueDate: $scope.formData.valid_until
                }

                ApiService.RegisterDelivery($scope.deliverFormData).then(
                    function (res) {
                        var data = res.data;
                        if (data.Status == 'OK') {
                            $('.num_selected').text(0);
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {
                        console.log(error, "can not get data.");
                    });
                //});
                $('#successModal').modal('hide');
                sessionStorage.clear();
                $state.go("app.orders.order.product");
                $('.ifDeliverSuccess').show(100);
                $timeout(function () {
                    $('.ifDeliverSuccess').hide(100);
                }, 2000);
            }


            $scope.getInternalDescription = function (itemList) {

                itemList.forEach(function (item) {
                    if ($scope.comparedItems.indexOf(item.OrderItemId) > -1) {
                        $scope.InternalDescription = item.InternalDescription;
                        //console.log($scope.DeliveryMtd);
                    }
                })
                return $scope.InternalDescription;
            }

            ApiService.getStatus($scope.orderItemId).then(
                function (res) {
                    $scope.OrderStatus = res.data.OrderStatus;

                    if ($scope.OrderStatus) {
                        $scope.status_id = res.data.OrderStatus[0].StatusId;
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );

            $scope.statusChangeModal = function () {
                $('#statusChangeConfirmation').modal('show');
            }

            $scope.updateStatusClick = function () {
                $('#statusChangeConfirmation').modal('hide');
                $scope.postUpdateStatus = {
                    OrderItemIds: [$scope.itemslist[0].OrderItemId]
                }

                ApiService.CompleteOrderItems($scope.postUpdateStatus).then(
                    function (res) {
                        var data = res.data;
                        if (data.Status == 'OK') {

                            $state.reload();
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {

                        console.log(error, "can not get data.");
                    }
                );


            };

        });
})();


(function () {
    angular
        .module("app")
        .controller("OrderEditCtr", ["$scope", "$http", "$state", "$stateParams", "$filter", "ApiService", "$timeout", "$window",
            function ($scope, $http, $state, $stateParams, $filter, ApiService, $timeout, $window) {
                var id = $stateParams.id;
                var shopCode = $stateParams.code1;
                $scope.FileID = null;
                $scope.param_code = shopCode;
                $scope.rStatusList = [];
                $scope.itemIdList = [];
                $scope.ShopCode = shopCode;
                $scope.reloadclick = function () {
                    $state.reload();
                }

                $scope.typeC = false;
                //$scope.typeMS = false;
                if (shopCode == "C") {
                    $scope.typeC = true;
                    //$scope.typeMS = false;
                    requestType = "Reproduction";
                    $scope.status_c = true;
                    $scope.status_s = false;
                    $scope.status_m = false;
                    $scope.status_p = false;
                } else if (shopCode == "S") {
                    $scope.typeC = false;
                    //$scope.typeMS = true;
                    requestType = "School  Request";
                    $scope.status_c = false;
                    $scope.status_s = true;
                    $scope.status_m = false;
                    $scope.status_p = false;
                } else if (shopCode == "M") {
                    $scope.typeC = false;
                    //$scope.typeMS = true;
                    requestType = "Marriage Request";
                    $scope.status_c = false;
                    $scope.status_s = false;
                    $scope.status_m = true;
                    $scope.status_p = false;
                } else if (shopCode == "P") {
                    $scope.typeC = false;
                    //$scope.typeMS = true;
                    requestType = "PictureSG Request";
                    $scope.status_c = false;
                    $scope.status_s = false;
                    $scope.status_m = true;
                    $scope.status_p = true;
                }

                $scope.trigCancel = function () {

                    if ($scope.statusSelected == "C" || $scope.statusSelected == "CPU") {
                        $scope.isTrigCancel = true;
                    } else {
                        $scope.isTrigCancel = false;
                    }
                }
                $scope.uploadloading = false;
                $(document).ready(function () {
                    $('#uploadloading').hide();
                    $("#fileUpload").on("change", function (e) {
                        $('.progress').show();
                        $scope.progress = 0;
                        $('#uploadloading').show();
                        // $event.stopPropagation();
                        //$scope.filesize = (this.files[0].size / 1024 / 1024).toFixed(2);
                        //if ($scope.filesize <= 0.01) {
                        //    $scope.filesize = "< 0.01mb";
                        //}
                        for (var i = 0; i <= this.files.length; i++) {

                            var filedata = new FormData();
                            filedata.append("File", this.files[i]);
                            filedata.append("OrderItemId", $stateParams.item_id);
                            filedata.append("FileUUID", "");
                            filedata.append("NasLocation", $scope.locSelect);
                            filedata.append("CreatedUserID", "ADMIN");
                            //filedata.append("File_Size", $scope.filesize);
                            //$scope.loading = true;

                            $.ajax({
                                "url": '/ARR/PublicApi/AttachmentApi/Upload',
                                "type": 'POST',
                                "data": filedata,
                                "cache": false,
                                "contentType": false,
                                "processData": false,
                                xhr: function () {
                                    var xhr = new window.XMLHttpRequest();
                                    //Upload progress
                                    xhr.upload.addEventListener("progress", function (evt) {
                                        if (evt.lengthComputable) {
                                            var percentComplete = evt.loaded / evt.total;
                                            //Do something with upload progress

                                            $scope.$apply(function () {
                                                $scope.progress = percentComplete;
                                            });
                                        }
                                    }, false);

                                    return xhr;
                                },
                                success: function (response) {
                                    getEditData();
                                    $('#uploadloading').hide();

                                },
                                error: function () {
                                    $(btn).text("Error").attr("disabled", "disabled");
                                    $window.alert(data.ErrorMessage);
                                }
                            });



                            //$scope.$watch(function () {
                            //    return $scope.progress;
                            //},
                            //    function (newValues, oldValues) {
                            //        console.log(newValues);
                            //        $scope.progress = newValues;
                            //    }
                            //);

                            //ApiService.AttachmentUploads(filedata)
                            //    .then(function (data) {
                            //        var data = data.data;
                            //            getEditData();
                            //            $('#uploadloading').hide();
                            //            if (data.Status === "ERROR" || data.Status === "FAILED") {
                            //                $(btn).text("Error").attr("disabled", "disabled");
                            //                $window.alert(data.ErrorMessage);
                            //            } else {
                            //                window.location.href = url;
                            //            }

                            //    });

                        }
                    });
                });


                // DeleteAttachment
                $scope.deleteFile = function (id) {

                    $scope.attachId = {
                        AttachId: id
                    }
                    ApiService.DeleteAttachment($scope.attachId).then(
                        function (r) {
                            var data = r.data;
                            if (data.Status == 'OK') {
                                getEditData();
                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }
                        },
                        function (error) {
                            console.log(error, "can not get data.");
                        }
                    );
                }

                $scope.locSelect = "Open";

                $scope.edit_item_id = {
                    OrderItemId: $stateParams.item_id
                };

                function pad(num) {
                    return ("0" + num).slice(-2);
                }

                function hhmmss(secs) {
                    var minutes = Math.floor(secs / 60);
                    secs = secs % 60;
                    var hours = Math.floor(minutes / 60)
                    minutes = minutes % 60;
                    //return '${pad(hours)}:${pad(minutes)}:${pad(secs)}';
                    return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
                }

                getEditData();

                function getEditData() {
                    ApiService.GetOrder($scope.edit_item_id).then(
                        function (m_res) {
                            var getQuotationItems = [];
                            var data = m_res.data;

                            if (data.Status == 'OK') {
                                $scope.item_ms = m_res.data.Orders;

                                $scope.item_ms.forEach(function (item_group) {
                                    $scope.quotation = item_group.Quotations;
                                    $scope.quotation.forEach(function (quot) {
                                        $scope.QuoteId = quot.QuoteId;
                                        $scope.quot_Items = quot.Items;

                                    })

                                    $scope.item_group = item_group.Items;
                                    $scope.item_group.forEach(function (item_output) {
                                        $scope.item = item_output;
                                        if ($stateParams.item_id == item_output.OrderItemId) {
                                            $scope.itemIdList.push(item_output.OrderItemId);
                                            $scope.item_details = item_output; //Steve 18/12/2018

                                            $scope.item_details.ActualStartTime = hhmmss($scope.item_details.ActualStartTime),
                                                $scope.item_details.ActualEndTime = hhmmss($scope.item_details.ActualEndTime);

                                        }
                                    }, this);
                                }, this);


                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }

                        },
                        function (error) {
                            console.log(error, "can not get data.");
                        }
                    );
                }

                $scope.itemIDforGroupItem = {
                    OrderId: $stateParams.id,
                    OrderItemId: $stateParams.item_id
                };
                ApiService.GetOrderItems($scope.itemIDforGroupItem).then(
                    function (item_r) {
                        var data_r = item_r.data;
                        $scope.groupItemId = [];
                        if (data_r.Status == 'OK') {
                            $scope.itemGroupList = data_r.OrderItems;
                            $scope.itemGroupList.forEach(function (item) {
                                $scope.groupItemId.push(item.OrderItemId);
                            });
                        } else {
                            $scope.errorMessage = data_r.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }

                    });


                $scope.openRejectModal = function () {
                    $('#itemRejectModal').modal('show');
                };

                $scope.detailsEditSubmit = function () {
                    $scope.formData = {
                        OrderItems: [{
                            OrderItemId: $scope.item_details.OrderItemId,
                            PublicComments: $scope.item_details.PublicComments,
                            InternalComments: $scope.item_details.InternalComments,
                            ERMSUrl: $scope.item_details.ERMSUrl,
                            //LastModifiedUserId: $scope.item_details.LastModifiedUserId,
                            ReproItem: {
                                StartTime: $scope.item_details.SelectedStartTime,
                                EndTime: $scope.item_details.SelectedEndTime,
                                ActualStartTime: moment($scope.item_details.ActualStartTime, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds'),
                                ActualEndTime: moment($scope.item_details.ActualEndTime, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds'),
                                ActualFirstWords: $scope.item_details.ActualFirstWords,
                                ActualLastWords: $scope.item_details.ActualLastWords,
                                Pages: $scope.item_details.SelectedPages,
                                ActualPages: $scope.item_details.ActualPages
                            }
                        }]
                    }

                    ApiService.UpdateOrderItem($scope.formData).then(
                        function (r) {
                            var data = r.data;
                            if (data.Status == 'OK') {
                                $window.scrollTo(0, 0);
                                $('#itemRejectModal').modal('hide');
                                getEditData();
                                //$state.go("app.orders.order.product");
                                $('.itemDetailsSavingSuccess').show(100);
                                $timeout(function () {
                                    $('.itemDetailsSavingSuccess').hide(100);
                                }, 2000);
                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }

                        },
                        function (error) {
                            console.log(error, "can not get data.");
                        }
                    );
                };

                $scope.getA2OFiles = function (orderItemID) {
                    $scope.loading = true;
                    $scope.getOrderItemId = {
                        OrderItemId: orderItemID
                    }
                    ApiService.GetA20Files($scope.getOrderItemId).then(function (r) {
                        var data = r.data;
                        if (data.Status == "OK") {
                            getEditData();
                            console.log('Attachments has been sent!');
                            $scope.loading = false;
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }

                    });
                }

                $scope.changeStatusCode = function () {
                    if ($scope.deliveryformData.deliveryStatus == "Delivery Generated") {
                        $scope.deliveryformData.statuscode = "PCL";
                    } else if ($scope.deliveryformData.deliveryStatus == "Delivered") {
                        $scope.deliveryformData.statuscode = "CM";
                    }
                };


                $scope.email_template_change = function (orderItemId) {
                    $scope.email_template = {
                        OrderItemId: orderItemId,
                        StatusId: $scope.email_template.StatusId
                    }
                    ApiService.GetEmailTemplate($scope.email_template).then(
                        function (r) {
                            var data = r.data;
                            if (data.Status == 'OK') {
                                $scope.email_template.Body = r.data.Body;
                                $scope.email_template.Subject = r.data.Subject;
                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }
                        },
                        function (error) {
                            console.log(error, "can not get data.");
                        }
                    );

                };

                ApiService.getStatus($scope.edit_item_id).then(
                    function (res) {
                        $scope.OrderStatus = res.data.OrderStatus;
                        if ($scope.OrderStatus) {
                            $scope.status_id = res.data.OrderStatus[0].StatusId;
                        }
                    },
                    function (error) {
                        console.log(error, "can not get data.");
                    }
                );

                $("#tooltipsBtn").hover(function () {
                    $(".tooltip_inner").toggle(100);
                });


                ApiService.GetOrderStatus($scope.edit_item_id).then(
                    function (res) {
                        var data = res.data;
                        if (data.Status == 'OK') {
                            $scope.rejectCode = [];
                            $scope.allStatus = [];
                            $scope.statusData = res.data.OrderStatus;

                            $scope.statusData.forEach(function (item) {
                                if (item.Sequence >= 900 && item.Sequence != 901) {
                                    if (item.StatusId) {
                                        $scope.email_template = {
                                            StatusId: item.StatusId
                                        }

                                    }
                                    $scope.rejectCode.push(item);

                                    $scope.email_template = {
                                        OrderItemId: $stateParams.item_id,
                                        StatusId: item.StatusId
                                    }
                                    ApiService.GetEmailTemplate($scope.email_template).then(
                                        function (r) {
                                            $scope.email_template.Body = r.data.Body;
                                            $scope.email_template.Subject = r.data.Subject;
                                        },
                                        function (error) {
                                            console.log(error, "can not get data.");
                                        }
                                    );
                                }
                            })
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {
                        console.log(error, "can not get data.");
                    }
                );

                $scope.rejectItemSend = function () {
                    $scope.email_template = {
                        OrderItemIds: $scope.groupItemId,
                        StatusId: $scope.email_template.StatusId,
                        LastModifiedUserId: $scope.item.LastModifiedUserId,
                        Subject: $scope.email_template.Subject,
                        Body: $scope.email_template.Body
                    }

                    ApiService.CancelOrderItem($scope.email_template).then(
                        function (r) {
                            var data = r.data;
                            if (data.Status == 'OK') {
                                $('#itemRejectModal').modal('hide');
                                $state.go("app.orders.order.product");
                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }
                        },
                        function (error) {
                            console.log(error, "can not get data.");
                        }
                    );
                };

                $scope.loading = false;

                $scope.statusChangeModal = function () {
                    $('#statusChangeConfirmation').modal('show');
                }

                $scope.updateStatusClick = function () {
                    $('#statusChangeConfirmation').modal('hide');
                    $scope.postUpdateStatus = {
                        OrderItemId: $scope.item_details.OrderItemId,
                        StatusId: $scope.status_id,
                        LastModifiedUserId: $scope.item_details.LastModifiedUserId
                    }

                    ApiService.UpdateStatus($scope.postUpdateStatus).then(
                        function (res) {
                            var data = res.data;
                            if (data.Status == 'OK') {
                                $state.reload();
                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }
                        },
                        function (error) {
                            console.log(error, "can not get data.");
                        }
                    );

                };


                $scope.getInvoiceValue = function (key, value) {
                    if (key == "itemIDs") {
                        return value.join("/");
                    } else return value;
                };

                $scope.getLabel = function (labelKey) {
                    var obj = {
                        orderStatus: "Order Status",
                        requestID: "Request ID",
                        requestorName: "Requestor's Name",
                        requestorEmail: "Requestor's Email",
                        mobile: "Mobile No.",
                        paymentMethod: "Payment Method",
                        no: "Invoice No.",
                        date: "Invoice Date",
                        paymentStatus: "Payment Status",
                        paymentDate: "Payment Date",
                        amount: "Invoice Amount",
                        itemIDs: "Item ID(s)",
                        remark: "Remarks"
                    };

                    return obj[labelKey];
                };
            }
        ]);
})();

(function () {

    angular.module('app.pages.orders.order.edit', [
        'ui.router',
    ]);

})();

(function () {
    angular.module("app.pages.orders.order.generate", [
        "ui.router",
        "ngAnimate",
        "ngSanitize",
        // "ui.bootstrap"
    ]);
})();


(function () {
    angular
        .module("app")
        .controller("OrderGenerateCtr", function ($scope, $stateParams, $state, $filter, $timeout, ApiService, $window) {
            $scope.title = $stateParams.subtitle;
            $scope.id = $stateParams.id;
            $scope.curr_date = new Date();
            $scope.sixtyDays = new Date();
            $scope.sixtyDays = $scope.sixtyDays.setMonth($scope.sixtyDays.getMonth() + 2);
            $scope.getItemslist = JSON.parse(sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY]);
            $scope.compareQuots = $scope.getItemslist.items;
            $scope.resultItems = [];

            var statusCode = $stateParams.code1;

            if (statusCode == "C") {
                $scope.status_c = true;
                $scope.status_s = false;
                $scope.status_m = false;
                $scope.status_p = false;
            } else if (statusCode == "S") {
                $scope.status_c = false;
                $scope.status_s = true;
                $scope.status_m = false;
                $scope.status_p = false;
            } else if (statusCode == "P") {
                $scope.status_c = false;
                $scope.status_s = false;
                $scope.status_m = true;
                $scope.status_p = false;
            } else {
                $scope.status_c = false;
                $scope.status_s = false;
                $scope.status_m = true;
                $scope.status_p = false;
            }

            $scope.requestid = {
                RequestId: $scope.id
            }

            ApiService.GetOrderDetails($scope.requestid).then(
                function (r) {
                    var data = r.data;
                    $scope.itemslist = [];
                    if (data.Status == 'OK') {
                        $scope.data = [];

                        $scope.compareQuots.forEach(function (quoteId) {

                            data.Orders.forEach(function (quote) {

                                if (quoteId == quote.QuoteId) {

                                    var orderItems = [];

                                    quote.Items.forEach(function (item) {

                                        if (item.QuoteType == 'Bundle' || !item.ItemGroupId) {
                                            orderItems = orderItems.concat(item.OrderItems);

                                        }

                                    });

                                    var bundlePrices = [];
                                    var itemPrices = [];
                                    var groupPrices = [];
                                    var gst = 0;
                                    var totalAmount = 0;
                                    quote.Items.forEach(function (item) {

                                        var orderCodes = [];
                                        var orderItemAANo = [];
                                        item.OrderItems.forEach(function (orderItem) {
                                            orderCodes.push(orderItem.ItemCode);
                                            orderItemAANo.push(orderItem.SourceNo);

                                        });

                                        if (item.ItemGroupId) {
                                            groupPrices.push({
                                                description: item.PriceRuleDescription,
                                                orderCodes: orderCodes,
                                                orderItemAA: orderItemAANo,
                                                amount: item.ActualItemAmount,
                                            });
                                        }

                                        if (item.ItemBundleId) {
                                            bundlePrices.push({
                                                description: item.PriceRuleDescription,
                                                orderCodes: orderCodes,
                                                amount: item.ActualItemAmount,
                                            });
                                        }

                                        if (!item.ItemBundleId && !item.ItemGroupId) {
                                            itemPrices.push({
                                                description: item.PriceRuleDescription,
                                                orderCodes: orderCodes,
                                                amount: item.ActualItemAmount,
                                            });
                                        }

                                        gst += item.ActualItemGSTAmount;
                                        totalAmount += item.ItemTotalAmount;
                                    });

                                    $scope.data.push({
                                        quoteId: quote.QuoteId,
                                        quoteCode: quote.QuoteCode,
                                        orderItems: orderItems,
                                        bundlePrices: bundlePrices,
                                        itemPrices: itemPrices,
                                        groupPrices: groupPrices,
                                        gst: gst,
                                        totalAmount: totalAmount
                                    });
                                    //return false;
                                }
                            });


                        });
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );

            $scope.invoiceConfirmModal = function () {
                $('#successModal').modal('show');
            }

            var sixtyDaysAfter = new Date($scope.sixtyDays);

            $scope.formData = {
                valid_date: $filter('date')($scope.curr_date, "yyyy-MM-dd"),
                valid_until: sixtyDaysAfter
            }

            $scope.isDisable = false;
            $scope.quotationFormSubmit = function () {
                $scope.isDisable = true;
                //$scope.compareQuots.forEach(function () {
                $scope.quoteFormData = {
                    QuoteIds: $scope.compareQuots,
                    DueDate: $scope.formData.valid_until
                }

                ApiService.GenerateInvoice($scope.quoteFormData).then(
                    function (res) {
                        var data = res.data;
                        if (data.Status == 'OK') {
                            $('#successModal').modal('hide');
                            sessionStorage.clear();
                            $state.go("app.orders.order.product");
                            $('.ifInvoiceSuccess').show(100);
                            $('.num_selected').text(0);
                            $timeout(function () {
                                $('.ifInvoiceSuccess').hide(100);
                            }, 2000);
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {
                        console.log(error, "can not get data.");

                    });
                //});
            }

            // console.log($scope.items);

            // $scope.openModal = function() {
            //     var modalInstance = $uibModal.open({
            //         templateUrl: "views/pages/orders/order/generate/modalContent.html",
            //         controller: "ModalContentCtrl",
            //         size: ""
            //     });

            //     modalInstance.result.then(function(response) {
            //         $scope.result = '$/{response}/ button hitted';
            //     });
            // };
        })
    // .controller("ModalContentCtrl", function($scope, $uibModalInstance) {
    //     $scope.ok = function() {
    //         $uibModalInstance.close("Ok");
    //     };

    //     $scope.cancel = function() {
    //         $uibModalInstance.dismiss();
    //     };
    // });
})();

(function () {
    angular
        .module("app")
        .controller("OrderInfoCtrl", function ($scope, $stateParams, $state, ApiService, $timeout, $window, $interval) {
            $scope.id = $stateParams.id;
            $scope.code2 = $stateParams.code2;
            var code2 = $stateParams.code2;

            $state.current.data = {
                back: true,
                backto: "app.orders.list"
            };


            $scope.getId = {
                RequestId: $scope.id,
                GroupCode: code2
            }

            $scope.printTaxInvoice = function (key) {
                $('.taxInvoiceModal' + key).modal('show');
            }
            $scope.openInvoice = function (key) {
                $('.InvoiceModal' + key).modal('show');

            }
            $scope.printTaxInvoiceForm = function (taxinv, add1, add2, add3) {
                $('.taxInvoiceModal').modal('hide');
                $scope.taxInvData = {
                    taxName: taxinv,
                    address1: add1,
                    address2: add2,
                    address3: add3
                }

                sessionStorage[TAX_INVOICE_INFO] = JSON.stringify($scope.taxInvData);
                $state.go("taxInvoices", {
                    "id": $scope.id,
                    "code2": $stateParams.code2
                });

            }

            $scope.cancelPrintTaxInvoice = function () {
                $('.taxInvoiceModal').modal('hide');
            }
            $scope.cancelInvoiceInfo = function () {
                $('.InvoiceModal').modal('hide');
            }

            ApiService.GetOrder($scope.getId).then(
                function (response) {
                    var data = response.data;

                    if (data.Status == 'OK') {
                        $scope.order = response.data.Orders[0];
                        $scope.groupCode = $scope.order.Items[0].GroupCode;
                        $scope.LastModifiedUserId = $scope.order.Items[0].LastModifiedUserId;
                        $scope.CategoryRouteCode = $scope.order.Items[0].CategoryRouteCode;
                        $scope.routeConfig = {
                            OrderItemGroupCode: $scope.groupCode,
                            CategoryRouteCode: $scope.CategoryRouteCode,
                            LastModifiedUserId: $scope.LastModifiedUserId
                        }

                        $scope.getGroupCode = {
                            GroupCode: $scope.groupCode
                        }
                        $scope.invoiceInfoList = [];
                        $scope.order.Invoices.forEach(function (eachInv) {
                            var getInvoice = {
                                InvoiceCode: eachInv.InvoiceCode,
                            }
                            ApiService.GetTaxInvoice(getInvoice).then(
                                function (inv_response) {

                                    var inv_data = inv_response.data;

                                    if (data.Status == 'OK') {

                                        var getInv = {
                                            invoices: eachInv,
                                            invoiceInfo: inv_data
                                        }
                                        $scope.invoiceInfoList.push(getInv);

                                    } else {
                                        $scope.errorMessage = data.ErrorMessage;
                                        $window.alert($scope.errorMessage);
                                    }
                                },
                                function (error) {
                                    console.log(error);
                                }
                            );
                        });


                        ApiService.GetPayment($scope.getGroupCode).then(
                            function (response) {
                                var data = response.data;

                                $scope.paymentInfoList = [];
                                if (data.Status == 'OK') {
                                    var tax_data = [];
                                    $scope.paymentData = response.data.RequestPayments;
                                    $scope.paymentData.forEach(function (payData) {
                                        var getTaxInvoice = {
                                            TaxInvoiceNo: payData.TaxInvoiceNo
                                        }
                                        ApiService.GetTaxInvoice(getTaxInvoice).then(
                                            function (tax_response) {
                                                var tax_data = tax_response.data;

                                                if (data.Status == 'OK') {
                                                    var taxInv = {
                                                        OrderCode: payData.OrderCode,
                                                        TaxInvoiceNo: payData.TaxInvoiceNo,
                                                        TransactionDate: payData.TransactionDate,
                                                        PaymentMethod: payData.PaymentMethod,
                                                        PaymentGateway: payData.PaymentGateway,
                                                        PaymentTotalAmount: payData.PaymentTotalAmount,
                                                        PaymentId: payData.PaymentId,
                                                        taxData: tax_data
                                                    }
                                                    $scope.paymentInfoList.push(taxInv);

                                                } else {
                                                    $scope.errorMessage = data.ErrorMessage;
                                                    $window.alert($scope.errorMessage);
                                                }
                                            },
                                            function (error) {
                                                console.log(error);
                                            }
                                        );
                                    });

                                } else {
                                    $scope.errorMessage = data.ErrorMessage;
                                    $window.alert($scope.errorMessage);
                                }
                            },
                            function (error) {
                                console.log(error);
                            }
                        );
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error);
                }
            );
            $scope.showRequestorInfo = false;
            var interval;
            $scope.request_reason = function (reason) {
                $scope.orderId = {
                    RequestId: $scope.id,
                    Reason: reason
                }
                if (reason) {
                    $scope.showRequestorInfo = true;
                    $scope.errorReason = "";

                    ApiService.ViewPersonalData($scope.orderId).then(
                        function (response) {
                            var data = response.data;
                            if (data.Status == 'OK') {
                                $scope.unmaskData = response.data;

                                interval = $interval(function () {
                                    console.log($scope.maskCount);
                                    if ($scope.maskCount > 1) {
                                        $scope.maskCount = $scope.maskCount - 1;
                                    } else {
                                        $scope.requestReason = '';
                                        $('#unmaskDetailsModal').modal('hide');
                                    }
                                }, 1000);

                            } else {
                                $scope.errorMessage = data.ErrorMessage;
                                $window.alert($scope.errorMessage);
                            }
                        },
                        function (error) {
                            console.log(error);
                        }
                    );
                } else {
                    $scope.showRequestorInfo = false;
                    $scope.errorReason = "Please key in your reason";
                }

            }
            $('#unmaskDetailsModal').on('hidden.bs.modal', function (e) {
                $interval.cancel(interval);
            });
            $scope.viewData = function () {
                $scope.showRequestorInfo = false;
                $scope.maskCount = 10;
                angular.element('#unmaskDetailsModal').modal('show');
            }


            ApiService.GetCategory().then(
                function (response) {
                    var data = response.data;
                    if (data.Status == 'OK') {
                        $scope.categories = response.data.category;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error);
                }
            );


            $scope.routeChange = function () {
                $('#routingSuccessModal').modal('show');
            }

            $scope.routeSend = function () {

                ApiService.UpdateOrderItemCategoryRoute($scope.routeConfig).then(
                    function (response) {
                        var data = response.data;
                        if (data.Status == 'OK') {
                            $('#routingSuccessModal').modal('hide');
                            $state.go("app.orders.list");
                            $('.routingDepartmentSuccess').show(100);
                            $timeout(function () {
                                $('.routingDepartmentSuccess').hide(100);
                            }, 2000);
                        } else {
                            $scope.errorMessage = data.ErrorMessage;
                            $window.alert($scope.errorMessage);
                        }
                    },
                    function (error) {
                        console.log(error);
                    }
                );
            }


            $scope.getLabel = function (labelKey) {
                var obj = {
                    Order_Status: "Order Status",
                    Request_ID: "Request ID",
                    Requestor_Name: "Requestor's Name",
                    Requestor_Email: "Requestor's Email",
                    Mobile_No: "Mobile No.",
                    Nationality: "Nationality",
                    Type_of_Use: "Type of Use",
                    Organization_Name: "Organization name/type",
                    UEN: "UEN",
                    Context_of_Use: "Context of Use",
                    Research_Topic: "Research_Topic",
                    Payment_Method: "Payment Method",
                    no: "Invoice No.",
                    date: "Invoice Date",
                    paymentStatus: "Payment Status",
                    paymentDate: "Payment Date",
                    amount: "Invoice Amount",
                    itemIDs: "Item ID(s)",
                    routing: "Route to"
                };

                return obj[labelKey];
            };
        });
})();

(function () {
    angular
        .module("app")
        .controller("TaxInvoicesCtrl", function ($scope, $stateParams, $state, ApiService, $timeout, $window) {
            $scope.id = $stateParams.id;
            var code2 = $stateParams.code2;
            if (!sessionStorage[TAX_INVOICE_INFO]) {
                $state.go('app.orders.order.info', {
                    "id": $scope.id,
                    "code2": $stateParams.code2
                });
            }

            var getTaxInfo = JSON.parse(sessionStorage[TAX_INVOICE_INFO]);

            $scope.tax_name = getTaxInfo.taxName;
            $scope.address_1 = getTaxInfo.address1;
            $scope.address_2 = getTaxInfo.address2;
            $scope.address_3 = getTaxInfo.address3;

            $scope.getId = {
                RequestId: $scope.id,
                GroupCode: code2
            }

            ApiService.GetOrder($scope.getId).then(
                function (response) {
                    var data = response.data;

                    if (data.Status == 'OK') {
                        $scope.order = response.data.Orders[0];
                        $scope.groupCode = $scope.order.Items[0].GroupCode;
                        $scope.LastModifiedUserId = $scope.order.Items[0].LastModifiedUserId;
                        $scope.CategoryRouteCode = $scope.order.Items[0].CategoryRouteCode;
                        $scope.routeConfig = {
                            OrderItemGroupCode: $scope.groupCode,
                            CategoryRouteCode: $scope.CategoryRouteCode,
                            LastModifiedUserId: $scope.LastModifiedUserId
                        }

                        $scope.getGroupCode = {
                            GroupCode: $scope.groupCode
                        }
                        $scope.invoiceInfoList = [];
                        $scope.order.Invoices.forEach(function (eachInv) {
                            var getInvoice = {
                                InvoiceCode: eachInv.InvoiceCode,
                            }
                            ApiService.GetTaxInvoice(getInvoice).then(
                                function (inv_response) {

                                    var inv_data = inv_response.data;

                                    if (data.Status == 'OK') {

                                        var getInv = {
                                            invoices: eachInv,
                                            invoiceInfo: inv_data
                                        }
                                        $scope.invoiceInfoList.push(getInv);
                                        sessionStorage.clear();
                                    } else {
                                        $scope.errorMessage = data.ErrorMessage;
                                        $window.alert($scope.errorMessage);
                                    }
                                },
                                function (error) {
                                    console.log(error);
                                }
                            );
                        });


                        ApiService.GetPayment($scope.getGroupCode).then(
                            function (response) {
                                var data = response.data;
                                $scope.paymentInfoList = [];
                                if (data.Status == 'OK') {
                                    var tax_data = [];
                                    $scope.paymentData = response.data.RequestPayments;
                                    $scope.paymentData.forEach(function (payData) {
                                        var getTaxInvoice = {
                                            TaxInvoiceNo: payData.TaxInvoiceNo
                                        }
                                        ApiService.GetTaxInvoice(getTaxInvoice).then(
                                            function (tax_response) {
                                                var tax_data = tax_response.data;

                                                if (data.Status == 'OK') {
                                                    var taxInv = {
                                                        OrderCode: payData.OrderCode,
                                                        TaxInvoiceNo: payData.TaxInvoiceNo,
                                                        TransactionDate: payData.TransactionDate,
                                                        PaymentMethod: payData.PaymentMethod,
                                                        PaymentGateway: payData.PaymentGateway,
                                                        PaymentTotalAmount: payData.PaymentTotalAmount,
                                                        taxData: tax_data
                                                    }
                                                    $scope.paymentInfoList.push(taxInv);

                                                } else {
                                                    $scope.errorMessage = data.ErrorMessage;
                                                    $window.alert($scope.errorMessage);
                                                }
                                            },
                                            function (error) {
                                                console.log(error);
                                            }
                                        );
                                    });

                                } else {
                                    $scope.errorMessage = data.ErrorMessage;
                                    $window.alert($scope.errorMessage);
                                }
                            },
                            function (error) {
                                console.log(error);
                            }
                        );
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error);
                }
            );
        });
})();

(function () {

    angular.module('app.pages.orders.order.info', [
        'ui.router',
    ]);

})();

(function () {

    angular.module('app').controller('OrderLogCtrl', function ($scope, $stateParams, $http, $state) {
        $scope.id = $stateParams.id;

        $state.current.data = {
            back: true,
            backto: 'app.orders.list'
        };

        $http.get("js/order_info.json")
            .then(function (response) {
                $scope.sampleData = response.data;

                $state.current.ncyBreadcrumb.label = 'Edit Details - #' + $scope.sampleData.generalInfomation.requestID;
            }, function (error) {
                console.log(error);
            });

    });

})();

angular
    .module("app")
    .controller("collectionsListCtrl", function ($scope, $rootScope, $stateParams, $http, $state, ApiService, $window, $timeout) {
        $scope.deliverys = [];

        //$scope.setPage = function (pageNo) {
        //    $scope.currentPage = pageNo;
        //};

        //$scope.pageChanged = function () {
        //    //console.log('Page changed to: ' + $scope.currentPage);
        //};

        //$scope.setItemsPerPage = function (num) {
        //    $scope.itemsPerPage = num;
        //    $scope.currentPage = 1; //reset to first page
        //}

        $scope.formFilter = {
            PageSize: 7,
            PageNumber: 1
        }
        $scope.pageNumber = 1;

        $scope.filterSubmit = function () {

            $scope.filtering = true;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: 1,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }

            $scope.ordersCount = $scope.count;
            //$scope.ordersLength = Math.round(parseInt($scope.ordersCount) / 7);

            getCollection($scope.formFilter);
        }

        $scope.addCount = function () {
            var num = parseInt(angular.element('#quantity').val()) + 1;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: num,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getCollection($scope.formFilter);
        }

        $scope.minusCount = function () {
            var num = parseInt(angular.element('#quantity').val()) - 1;

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: num,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getCollection($scope.formFilter);
        }


        $scope.pageNumberChange = function () {

            $scope.formFilter = {
                PageSize: 7,
                PageNumber: $scope.pageNumber,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }

            getCollection($scope.formFilter);
        }

        $scope.firstPage = function () {
            angular.element('#quantity').val(1);
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: 1,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getCollection($scope.formFilter);
        }

        $scope.lastPage = function () {
            angular.element('#quantity').val($scope.ordersLength);
            $scope.formFilter = {
                PageSize: 7,
                PageNumber: $scope.ordersLength,
                FromDate: $scope.formFilter.FromDate ? $scope.formFilter.FromDate : null,
                ToDate: $scope.formFilter.ToDate ? $scope.formFilter.ToDate : null,
                CategoryCode: $scope.formFilter.CategoryCode ? $scope.formFilter.CategoryCode : null,
                StatusIds: $scope.formFilter.StatusIds ? $scope.formFilter.StatusIds : null,
                ItemCode: $scope.formFilter.ItemCode ? $scope.formFilter.ItemCode : null,
                RequestorName: $scope.formFilter.RequestorName ? $scope.formFilter.RequestorName : null
            }
            getCollection($scope.formFilter);
        }

        $scope.openRemarksModal = function (index) {
            $scope.remarksDefault = 'Item collected at ' + moment().format("hh:mma YYYY-MM-DD") + ' from staff ' + $rootScope.username;
            $scope.remarkModalIndex = index;
            $('.remarkModal-' + index).modal('show');

        }

        // $scope.updateStatusModal = function(itemId) {
        //     $scope.getItemId = itemId;
        //     $('#itemCompleteConfirmation').modal('show');
        // }

        $scope.updateRemark = function (itemId, comment) {
            $scope.postUpdateStatus = {
                OrderItemIds: [itemId]
            }
            $scope.deliverFormData = {
                OrderItemId: itemId,
                InternalComments: comment
            }
            $('.remarkModal-' + $scope.remarkModalIndex).modal('hide');


            ApiService.CompleteOrderItems($scope.postUpdateStatus).then(
                function (res) {
                    var data = res.data;
                    if (data.Status == 'OK') {
                        //getCollection($scope.formFilter);
                        //$state.reload();
                        ApiService.UpdateDelivery($scope.deliverFormData).then(
                            function (res) {
                                var data = res.data;
                                if (data.Status == 'OK') {
                                    getCollection($scope.formFilter);
                                    //sessionStorage.clear();
                                    //$state.go("app.orders.delivery.list");

                                    $('.ifDeliverUpdateSuccess').show(100);
                                    $timeout(function () {
                                        $('.ifDeliverUpdateSuccess').hide(100);
                                    }, 2000);
                                } else {
                                    $scope.errorMessage = data.ErrorMessage;
                                    $window.alert($scope.errorMessage);
                                }
                            },
                            function (error) {
                                console.log(error, "can not get data.");
                            });
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }
                },
                function (error) {

                    console.log(error, "can not get data.");
                }
            );

        }

        // $scope.updateStatusClick = function() {

        //     $scope.postUpdateStatus = {
        //         OrderItemIds: [$scope.getItemId]
        //     }

        //     ApiService.CompleteOrderItems($scope.postUpdateStatus).then(
        //         function(res) {
        //             var data = res.data;
        //             if (data.Status == 'OK') {
        //                 $('#itemCompleteConfirmation').modal('hide');
        //                 getCollection($scope.formFilter);
        //                 //$state.reload();
        //             } else {
        //                 $scope.errorMessage = data.ErrorMessage;
        //                 $window.alert($scope.errorMessage);
        //             }
        //         },
        //         function(error) {

        //             console.log(error, "can not get data.");
        //         }
        //     );


        // };

        $scope.setShopCode = function (code) {
            $scope.gettingStatus = true;
            $scope.statusList = [];

            ApiService.GetCategory().then(
                function (response) {
                    var data = response.data;
                    if (data.Status == 'OK') {
                        $scope.compare_cate = response.data.category;
                        $scope.compare_cate.forEach(function (cate) {

                            if (cate.CategoryCode == code) {

                                $scope.getShopCode = cate.ShopCode;
                            }
                        })
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error);
                }
            );

            ApiService.GetOrderStatus().then(
                function (res) {
                    var data = res.data;
                    if (data.Status == 'OK') {
                        //$scope.rejectCode = [];
                        //$scope.allStatus = [];
                        $scope.status_compare = res.data.OrderStatus;
                        $scope.status_compare.forEach(function (status) {
                            if (status.ShopCode == $scope.getShopCode || !status.ShopCode) {
                                $scope.statusList.push(status);
                            }
                        })
                        $scope.gettingStatus = false;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );


        }


        $scope.filter = {
            route: ''
        }

        ApiService.GetCategory().then(
            function (response) {
                var data = response.data;
                if (data.Status == 'OK') {

                    $scope.categories = response.data.category;
                } else {
                    $scope.errorMessage = data.ErrorMessage;
                    console.log($scope.errorMessage);
                }
            },
            function (error) {
                console.log(error);
            }
        );

        getCollection($scope.formFilter);

        function getCollection(filterData) {

            ApiService.GetDeliveryCollection(filterData).then(
                function (r) {
                    var data = r.data;
                    if (data.Status == 'OK') {
                        $scope.deliverys = r.data.RequestDelv;
                        $scope.count = $scope.deliverys.length;
                        $scope.ordersLength = r.data.TotalPages
                        //$scope.viewby = 10;
                        //$scope.totalItems = $scope.deliverys.length;
                        //$scope.currentPage = 1;
                        //$scope.itemsPerPage = $scope.viewby;
                        //$scope.maxSize = 5;
                        $scope.filtering = false;
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        console.log($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );

        }

        // $scope.toggleAll = () => {
        //     var toggleStatus = $scope.isAllSelected;
        //     $scope.deliverys.forEach(function(itm) {
        //         itm.selected = toggleStatus;
        //     }, this);
        // };

        // $scope.optionToggled = item => {

        //     $scope.isAllSelected = $scope.deliverys.every(function(itm) {
        //         return itm.selected;
        //     });
        // };

        $scope.generatedeliver = function (OrderId, GroupCode) {
            $state.go("app.orders.order.deliver", {
                'id': OrderId,
                'code1': GroupCode.charAt(0),
                'code2': GroupCode
            });
        }

        $scope.generateDelivery = function (OrderId, objId, GroupCode, devId) {

            $scope.itemss = {
                requestcode: OrderId,
                items: [objId],
                from: "deliveryList",
                deliverId: devId
            };

            sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY] = JSON.stringify($scope.itemss);
            $scope.generatedeliver(OrderId, GroupCode);
        }
    }).directive('lgtSearchCollections', lgtSearchCollections);

function lgtSearchCollections() {
    return {
        templateUrl: 'views/components/lgt-search-collections.html'
    }
}

angular
    .module("app").controller("excelCtrl", function ($state, $scope, $stateParams, ApiService, $window) { });

angular
    .module("app").controller("excelDeliveryReportCtrl", function ($state, $scope, $stateParams, ApiService, $window) {

        $scope.excelData = {
            FromDate: '',
            ToDate: ''
        };

        $scope.getFileName = function () {
            var str = ('Delivery Report -') + " From " + ($scope.excelData.FromDate ? moment($scope.excelData.FromDate).format("YYYY-MM-DD") : 'beginning') + " To " + ($scope.excelData.ToDate ? moment($scope.excelData.ToDate).format("YYYY-MM-DD") : 'now') + ".csv";
            return str;
        }
        $scope.csvHeader = [
            'No',
            'Item Code',
            'Category Code',
            'Requestor Name',
            'Method Description',
            'Method Instruction',
            'Internal Description',
            'Format',
            'Created Date',
            'Due Date',
            'LastModified Date'
        ];
        //GetDeliveryReport();

        function GetDeliveryReport(data) {

            ApiService.GetDeliveryReport(data).then(
                function (response) {
                    var data = response.data;
                    console.log(data);
                    $scope.csvData = [];
                    if (data.Status == 'OK') {
                        var count = 1;
                        $scope.DeliveryOrders = response.data.DeliveryOrders;

                        $scope.viewby = 7;
                        $scope.totalItems = $scope.DeliveryOrders.length;
                        $scope.currentPage = 1;
                        $scope.itemsPerPage = $scope.viewby;
                        $scope.maxSize = 5;

                        $scope.DeliveryOrders.forEach(function (item) {
                            var CreatedDate = item.CreatedDate ? moment(item.CreatedDate).format("YYYY-MM-DD HH:mm a") : '';
                            var DueDate = item.DueDate ? moment(item.DueDate).format("YYYY-MM-DD HH:mm a") : '';
                            var LastModifiedDate = item.LastModifiedDate ? moment(item.LastModifiedDate).format("YYYY-MM-DD HH:mm a") : '';
                            $scope.csvData.push({
                                a: count,
                                b: item.ItemCode,
                                c: item.CategoryCode,
                                d: item.RequestorName,
                                e: item.MethodDescription,
                                f: item.MethodInstruction,
                                g: item.InternalDescription,
                                h: item.Format,
                                i: CreatedDate,
                                j: DueDate,
                                k: LastModifiedDate
                            })
                            count++;
                        });
                        $scope.csvOrder = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'];
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                });
        }


        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            //console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.setItemsPerPage = function (num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1; //reset to first page
        }

        $scope.dataSubmit = function () {
            $scope.csvData = [];
            $scope.DeliveryOrders = [];
            var date_from = moment($scope.excelData.FromDate).format("YYYY-MM-DD");
            var date_to = moment($scope.excelData.ToDate).format("YYYY-MM-DD");
            $scope.excelData = {
                FromDate: new Date(date_from),
                ToDate: new Date(date_to)
            };
            GetDeliveryReport($scope.excelData);
        }
    });

angular
    .module("app").controller("excelExportCtrl", function ($state, $scope, $stateParams, ApiService, $window) {

        $scope.excelData = {
            FromDate: '',
            ToDate: ''
        };


        $scope.excelCategory = 'pt';

        $scope.getFileName = function () {
            var str = ($scope.excelCategory == 'rs' ? 'Transaction Summary -' : 'Payment Transaction Report - ') + " From " + ($scope.excelData.FromDate ? moment($scope.excelData.FromDate).format("YYYY-MM-DD") : 'beginning') + " To " + ($scope.excelData.ToDate ? moment($scope.excelData.ToDate).format("YYYY-MM-DD") : 'now') + ".csv";
            return str;
        }

        $scope.selectCategory = function () {
            $scope.TransSummary = [];
            $scope.PaymentTrans = [];
            if ($scope.excelCategory == 'rs') {
                //reportSummaryData();
            } else if ($scope.excelCategory == 'pt') {
                //reportPaymentTansData();
            }
        }

        function reportSummaryData(data) {
            $scope.csvHeader = [
                'No',
                'Terminal ID',
                'Order Code',
                'Transaction Ref',
                'Payment Method',
                'Amount Before GST',
                'Actual GST Amount',
                'Actual Item Amount',
                'Transaction Date'
            ];

            ApiService.GetTransSummary(data).then(
                function (response) {
                    var data = response.data;

                    $scope.csvData = [];
                    if (data.Status == 'OK') {
                        var count = 1;
                        $scope.TransSummary = response.data.TransSummary;

                        $scope.viewby = 7;
                        $scope.totalItems = $scope.TransSummary.length;
                        $scope.currentPage = 1;
                        $scope.itemsPerPage = $scope.viewby;
                        $scope.maxSize = 5;

                        $scope.TransSummary.forEach(function (item) {
                            $scope.csvData.push({
                                z: count,
                                a: item.TerminalID,
                                b: item.OrderCode,
                                c: item.TransactionRef,
                                d: item.PaymentMethod,
                                e: item.AmountBeforeGST,
                                f: item.ActualGSTAmount,
                                g: item.ActualItemAmount,
                                h: item.TransactionDate
                            })
                            count++;
                        });
                        $scope.csvOrder = ['z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );
        }

        //reportPaymentTansData();

        function reportPaymentTansData(data) {
            $scope.csvHeader = [
                'No',
                'Request No.',
                'Quotation Item ID',
                'Order No.',
                'Source No.',
                'Transaction ID',
                'Tax Invoice No.',
                'Collection Date',
                'Revenue Category / GL Code (for reports before 1 June)',
                'Service Code',
                'Item Desc',
                'Item Desc 2',
                'Fund Code',
                'Amount before GST',
                'Add: GST',
                'Amount after GST',
                'Less: Charges',
                'Amount received in Bank',
                'Merchant / Terminal ID',
                'Payment Mode',
                'PAYPAL',
                'CashCard',
                'NETS',
                'EZLINK'
            ];

            ApiService.GetPaymentTransaction(data).then(
                function (response) {
                    var data = response.data;
                    $scope.csvData = [];
                    if (data.Status == 'OK') {
                        var count = 1;
                        $scope.PaymentTrans = response.data.PaymentTransactions;

                        $scope.viewby = 7;
                        $scope.totalItems = $scope.PaymentTrans.length;
                        $scope.currentPage = 1;
                        $scope.itemsPerPage = $scope.viewby;
                        $scope.maxSize = 5;

                        $scope.PaymentTrans.forEach(function (item) {

                            $scope.csvData.push({
                                z: count,
                                y: item.RequestNo,
                                x1: item.QuoteItemId,
                                x: item.OrderNo,
                                w1: item.SourceNo,
                                w: item.TransactionID,
                                a: item.InvoiceNo,
                                b: item.CollectionDate,
                                c: item.GLAccount,
                                d: item.ServiceCode,
                                e: item.ItemDescription,
                                f1: item.ItemDescription2,
                                f: item.FundCode,
                                g: item.AmountBeforeGST,
                                h: item.GSTAmount,
                                i: item.AmountAfterGST,
                                j: item.LessCharges,
                                k: item.AmountReceivedInBank,
                                l: item.TerminalID,
                                m: item.PaymentMode,
                                n: item.PAYPAL,
                                o: item.PAYNOW,
                                p: item.NETS,
                                q: item.EZLINK
                            })
                            count++;
                        });
                        $scope.csvOrder = ['z', 'y', 'x1', 'x', 'w1', 'w', 'a', 'b', 'c', 'd', 'e', 'f1', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q'];
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );
        }

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            //console.log('Page changed to: ' + $scope.currentPage);
        };

        $scope.setItemsPerPage = function (num) {
            $scope.itemsPerPage = num;
            $scope.currentPage = 1; //reset to first page
        }

        $scope.dataSubmit = function () {
            $scope.csvData = [];
            $scope.TransSummary = [];
            $scope.PaymentTrans = [];
            var date_from = moment($scope.excelData.FromDate).format("YYYY-MM-DD");
            var date_to = moment($scope.excelData.ToDate).format("YYYY-MM-DD");
            $scope.excelData = {
                FromDate: new Date(date_from),
                ToDate: new Date(date_to)
            };

            if ($scope.excelCategory == 'rs') {
                reportSummaryData($scope.excelData);
            } else if ($scope.excelCategory == 'pt') {
                reportPaymentTansData($scope.excelData);
            }


        }

    });

angular.module("app").controller("excelOrderReportCtrl", function ($state, $scope, $stateParams, ApiService, $window) {

    $scope.excelData = {
        FromDate: '',
        ToDate: ''
    };

    $scope.excelCategory = 'os';

    $scope.getFileName = function () {
        var str = ($scope.excelCategory == 'os' ? 'Order Summary Report-' : $scope.excelCategory == 'od' ? 'Order Details Report - ' : 'Attachment Download Log') + " From " + ($scope.excelData.FromDate ? moment($scope.excelData.FromDate).format("YYYY-MM-DD") : 'beginning') + " To " + ($scope.excelData.ToDate ? moment($scope.excelData.ToDate).format("YYYY-MM-DD") : 'now') + ".csv";
        return str;
    }

    $scope.selectCategory = function () {
        $scope.OrderSummary = [];
        $scope.OrderDetails = [];
        $scope.AttachDownloadLogs = [];
        if ($scope.excelCategory == 'os') {
            //reportOrderSummary();
        } else if ($scope.excelCategory == 'od') {
            //reportOrderDetails();
        } else if ($scope.excelCategory == 'ad') {
            //AttachDownloadLogReport();
        }
    }
    //reportOrderSummary();

    function reportOrderSummary(data) {
        $scope.csvHeader = [
            'No.',
            'Order #',
            'Requestor Name',
            'Type of Order',
            'Context of Use',
            'Nationality',
            'Purpose of Use',
            'Last Modified',
            'Created On',
            'Total no. of items',
            'Total no. of physical items',
            'No. of Item with Open Status (Patron-view)',
            'No. of Pending Verification (Patron - view)',
            'No. of Item Processing(Patron - view)',
            'No. of Item Pending Payment (Patron-view)',
            'No. of Item Pending Verification (Patron-view)',
            'No. of Item Pending Collection (Patron-view)',
            'No. of Item Completed (Patron-view)',
            'No. of Item Cancelled (Patron-view)',
            'No. of Item with other Status (Patron-view)'
        ];

        ApiService.GetOrderSummary(data).then(
            function (response) {
                var data = response.data;

                $scope.csvData = [];
                if (data.Status == 'OK') {
                    var count = 1;
                    $scope.OrderSummary = response.data.OrderSummary;

                    $scope.viewby = 7;
                    $scope.totalItems = $scope.OrderSummary.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.maxSize = 5;

                    $scope.OrderSummary.forEach(function (item) {
                        $scope.csvData.push({
                            a: count,
                            b: item.OrderCode,
                            c: item.RequestorName,
                            d: item.TypeofOrder,
                            e: item.ContextofUse,
                            f: item.Nationality,
                            g: item.PurposeofUse,
                            h: item.ModifedDate,
                            i: item.CreatedDate,
                            j: item.ItemsCount,
                            k: item.PhysicalItemsCount,
                            l: item.Open,
                            m: item.Verifying_Request,
                            n: item.Processing,
                            o: item.PendingPayment,
                            p: item.PendingVerification,
                            q: item.PendingCollectionViewing,
                            r: item.Completed,
                            s: item.Cancelled,
                            t: item.CompletedResultNotFound
                        })
                        count++;
                    });
                    $scope.csvOrder = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'];
                } else {
                    $scope.errorMessage = data.ErrorMessage;
                    $window.alert($scope.errorMessage);
                }

            },
            function (error) {
                console.log(error, "can not get data.");
            }
        );
    }

    function reportOrderDetails(data) {
        $scope.csvHeader = [
            'No.',
            'Item #',
            'Work Order #',
            'Category',
            'Requestor Name',
            'Context of Use',
            'Nationality',
            'Purpose of Use',
            'Rejection Reason',
            'Organisation',
            'Research Topic',
            'Last Modified',
            'Created On',
            'Source No.',
            'Record Title',
            'Format',
            'Record Status (internal)',
            'Record Status (Public)',
            'Payment Status',
            'Remarks'
        ];

        ApiService.GetOrderDetail(data).then(
            function (response) {
                var data = response.data;
                $scope.csvData = [];
                if (data.Status == 'OK') {
                    var count = 1;
                    $scope.OrderDetails = response.data.OrderDetails;

                    $scope.viewby = 7;
                    $scope.totalItems = $scope.OrderDetails.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.maxSize = 5;

                    $scope.OrderDetails.forEach(function (item) {
                        $scope.csvData.push({
                            a: count,
                            b: item.ItemCode,
                            c: item.GroupCode,
                            d: item.CategoryCode,
                            e: item.RequestorName,
                            f: item.ContextofUse,
                            g: item.Nationality,
                            h: item.PurposeofUse,
                            i: item.RejectionReason,
                            j: item.Organisation,
                            k: item.ResearchTopic,
                            l: item.LastModifiedUserId,
                            m: item.CreatedDate,
                            n: item.SourceNo,
                            o: item.SourceTitle,
                            p: item.Format,
                            q: item.InternalDescription,
                            r: item.PublicDescription,
                            s: item.PaymentStatus,
                            t: item.Remarks
                        })
                        count++;
                    });
                    $scope.csvOrder = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't'];
                } else {
                    $scope.errorMessage = data.ErrorMessage;
                    $window.alert($scope.errorMessage);
                }

            },
            function (error) {
                console.log(error, "can not get data.");
            }
        );
    }

    function AttachDownloadLogReport(data) {
        $scope.csvHeader = [
            'No.',
            'Order Item ID',
            'Order ID',
            'Record Title',
            'Attachment Filename',
            'Format',
            'Download Date / Time',
            'Download Location'
        ];

        ApiService.GetAttachDownloadLog(data).then(
            function (response) {
                var data = response.data;
                $scope.csvData = [];
                if (data.Status == 'OK') {
                    var count = 1;
                    $scope.AttachDownloadLogs = response.data.AttachDownloadLogs;

                    $scope.viewby = 7;
                    $scope.totalItems = $scope.AttachDownloadLogs.length;
                    $scope.currentPage = 1;
                    $scope.itemsPerPage = $scope.viewby;
                    $scope.maxSize = 5;

                    $scope.AttachDownloadLogs.forEach(function (item) {

                        var date = item.DownloadDate ? moment(item.DownloadDate).format("YYYY-MM-DD HH:mm a") : '';
                        $scope.csvData.push({
                            z: count,
                            a: item.ItemCode,
                            b: item.OrderCode,
                            c: item.SourceTitle,
                            d: item.Filename,
                            e: item.FileFormat,
                            f: date,
                            g: item.DownloadLocation
                        })
                        count++;
                    });
                    $scope.csvOrder = ['z', 'a', 'b', 'c', 'd', 'e', 'f', 'g'];
                } else {
                    $scope.errorMessage = data.ErrorMessage;
                    $window.alert($scope.errorMessage);
                }

            },
            function (error) {
                console.log(error, "can not get data.");
            }
        );
    }

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function () {
        //console.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.setItemsPerPage = function (num) {
        $scope.itemsPerPage = num;
        $scope.currentPage = 1; //reset to first page
    }

    $scope.dataSubmit = function () {
        $scope.csvData = [];
        $scope.OrderSummary = [];
        $scope.OrderDetails = [];
        $scope.AttachDownloadLogs = [];
        var date_from = moment($scope.excelData.FromDate).format("YYYY-MM-DD");
        var date_to = moment($scope.excelData.ToDate).format("YYYY-MM-DD");
        $scope.excelData = {
            FromDate: new Date(date_from),
            ToDate: new Date(date_to)
        };

        if ($scope.excelCategory == 'os') {
            reportOrderSummary($scope.excelData);
        } else if ($scope.excelCategory == 'od') {
            reportOrderDetails($scope.excelData);
        } else if ($scope.excelCategory == 'ad') {
            AttachDownloadLogReport($scope.excelData);
        }


    }
});

angular
    .module("app").controller("OrderProductCtrl", function ($state, $scope, $stateParams, ApiService, $window) {
        var id = $stateParams.id;
        var code = $stateParams.code1;
        var code2 = $stateParams.code2;

        $scope.isGenerate = true;
        $scope.code = code;
        $scope.id = id;
        $scope.items = [];
        $scope.deliveryItems = [];

        var work_OrderCode = $stateParams.code2;
        if (work_OrderCode) {
            if (work_OrderCode.indexOf("SA-") >= 0 || work_OrderCode.indexOf("S-") >= 0 || work_OrderCode.indexOf("AP-") >= 0) {
                angular.element(".lgt-btn-generate-quotation").show();
            } else {
                angular.element(".lgt-btn-generate-quotation").hide();
            }
        }

        $scope.myRequest = {
            RequestId: $scope.id,
            GroupCode: code2
        };

        if (work_OrderCode.indexOf("VO-") >= 0) {
            $scope.hasVO = true;
            GetOrderByRequest()
        } else {
            $scope.hasVO = false;
            getOrderDetails();
        }

        function pad(num) {
            return ("0" + num).slice(-2);
        }

        function hhmmss(secs) {
            var minutes = Math.floor(secs / 60);
            secs = secs % 60;
            var hours = Math.floor(minutes / 60)
            minutes = minutes % 60;
            //return `${pad(hours)}:${pad(minutes)}:${pad(secs)}`;
            return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
        }

        function getOrderDetails() {
            ApiService.GetOrderDetails($scope.myRequest).then(
                function (res) {
                    var data = res.data;
                    if (data.Status == 'OK') {
                        $scope.editDateTimeList = [];
                        $scope.get_status = [];
                        var theKey = 0;
                        var numPD = 0;
                        var numPQ = 0;
                        $scope.get_orderitems = res.data.Orders;

                        $scope.get_orderitems.forEach(function (item) {
                            $scope.orderitems_output = item.Items;

                            $scope.orderitems_output.forEach(function (gitem) {
                                // for (var i = 0; i < gitem.OrderItems.length; i++) {

                                //     var momentActualStart = gitem.OrderItems[i].ActualStartTime;
                                //     var momentActualEnd = gitem.OrderItems[i].ActualEndTime;

                                //     var getDateTime = {
                                //         time: {
                                //             actualStartTime: hhmmss(momentActualStart),
                                //             actualEndTime: hhmmss(momentActualEnd),

                                //         }
                                //     };
                                //     $scope.editDateTimeList.push(getDateTime);

                                // }

                                if (gitem.QuoteType == 'Item' || gitem.QuoteType == 'Bundle') {

                                    gitem.OrderItems.forEach(function (get) {

                                        //get.ActualDuration = moment.duration(get.ActualDuration, 'seconds');

                                        $scope.get_status[theKey] = get.StatusCode;

                                        if (get.StatusCode == 'PQ') {
                                            numPQ++;
                                        }
                                        if (get.StatusCode == 'PD') {
                                            numPD++;
                                        }
                                        theKey++;
                                    });
                                }


                            })

                            $('.numPQ').text(numPQ);
                            $('.numPD').text(numPD);

                            $scope.Order_Remarks = item.OrderRemarks;
                        }, this);

                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );
        }

        function GetOrderByRequest() {
            ApiService.GetOrder($scope.myRequest).then(
                function (fe_res) {
                    var data = fe_res.data;
                    if (data.Status == 'OK') {
                        var numPD = 0;
                        var numPQ = 0;
                        $scope.get_orderitems = fe_res.data.Orders;
                        $scope.get_orderitems.forEach(function (item) {
                            $scope.quotations = item.Quotations;
                            $scope.quotations.forEach(function (quot) {
                                $scope.quot_items = quot.Items;

                                //--------Order Items---------------------

                                ApiService.GetOrder($scope.myRequest).then(
                                    function (r) {
                                        $scope.get_quoteitems = r.data.Orders;
                                        $scope.get_quoteitems.forEach(function (item) {
                                            $scope.quotation_items = item.Items;
                                        }, this);
                                    },
                                    function (error) {
                                        console.log(error, "can not get data.");
                                    }
                                );

                            })
                            $scope.orderitems_output = item.Items;
                            $scope.orderitems_output.forEach(function (get) {
                                $scope.get_status = get.StatusCode;
                                if (get.StatusCode == 'PQ') {
                                    numPQ++;
                                }
                                if (get.StatusCode == 'PD') {
                                    numPD++;
                                }
                            })

                            $('.numPQ').text(numPQ);
                            $('.numPD').text(numPD);

                            $scope.Order_Remarks = item.OrderRemarks;
                        }, this);
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }
                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );
        }

        $scope.StartEndTimeModal = function (id, startTime, endTime) {
            $('#' + id).modal('show');
            $scope.editStartTime = hhmmss(startTime);
            $scope.editEndTime = hhmmss(endTime);
        }

        $scope.updateItemInfo = function (id, orderItemId, starttime, endtime, actualStart, actualEnd) {

            if (typeof actualStart == 'number') {
                actualStart = hhmmss(actualStart);
            }
            if (typeof actualEnd == 'number') {
                actualEnd = hhmmss(actualEnd);
            }

            $scope.formData = {
                OrderItems: [{
                    OrderItemId: orderItemId,
                    ReproItem: {
                        StartTime: starttime,
                        EndTime: endtime,
                        ActualStartTime: moment(actualStart, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds'),
                        ActualEndTime: moment(actualEnd, 'HH:mm:ss').diff(moment().startOf('day'), 'seconds')
                    }

                }]
            }

            ApiService.UpdateOrderItem($scope.formData).then(
                function (r) {
                    var data = r.data;
                    if (data.Status == 'OK') {
                        //$('#itemRejectModal').modal('hide');
                        $('#' + id).modal('hide');
                        getOrderDetails();
                    } else {
                        $scope.errorMessage = data.ErrorMessage;
                        $window.alert($scope.errorMessage);
                    }

                },
                function (error) {
                    console.log(error, "can not get data.");
                }
            );

        }

        $scope.editOrder = function (id) {
            $state.go("app.orders.order.edit", {
                "item_id": id
            });
        };

        //$scope.toggleAll = function (items) {
        //    var toggleStatus = $scope.isAllSelected;
        //    $scope.itemsData = items;

        //    if ($scope.isAllSelected === true) {
        //        $scope.itemsData.forEach(function (itm) {
        //            if (itm.StatusCode == 'PD') {
        //                $scope.items.push(itm.OrderItemId);
        //                itm.selected = toggleStatus;
        //            }

        //        }, this);

        //    } else {
        //        $scope.items = [];
        //        $scope.itemsData.forEach(function (itm) {
        //            itm.selected = toggleStatus;
        //        }, this);
        //    }

        //    $scope.itemss = {
        //        selected: "true",
        //        requestcode: id,
        //        items: $scope.items,
        //        from: "ProductDetails"
        //    };

        //    sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY] = JSON.stringify($scope.itemss);

        //    $('.num_selected').text($scope.itemss.items.length);
        //    if ($scope.itemss.items.length == 0) {
        //        $('.chk-btn-generate-quotation').prop('disabled', true);
        //        $('.chk-btn-deliver').prop('disabled', true);
        //    } else {
        //        $('.chk-btn-generate-quotation').prop('disabled', false);
        //        $('.chk-btn-deliver').prop('disabled', false);
        //    }
        //};

        $scope.optionToggled = function (item) {

            var removeDeliveryItem = [];

            if (item.selected === true) {
                $scope.deliveryItems.push(item.OrderItemId);
            } else {
                removeDeliveryItem.push(item.OrderItemId);

                var i = $scope.deliveryItems.length;

                while (i--) {
                    if (removeDeliveryItem.indexOf($scope.deliveryItems[i]) != -1) {
                        $scope.deliveryItems.splice(i, 1);
                    }
                }
            }

            $scope.itemss = {
                selected: "true",
                requestcode: id,
                items: $scope.deliveryItems,
                from: "ProductDetails"
            };

            sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY] = JSON.stringify($scope.itemss);
            $scope.isAllSelected = $scope.items.every(function (itm) {
                return itm.selected;
            });

            $('.num_selected').text($scope.itemss.items.length);
            if ($scope.itemss.items.length == 0) {
                $('.chk-btn-generate-quotation').prop('disabled', true);
                $('.chk-btn-deliver').prop('disabled', true);
            } else {
                $('.chk-btn-generate-quotation').prop('disabled', false);
                $('.chk-btn-deliver').prop('disabled', false);
            }
        };



        // Generate Invoice Selection toggle function //
        //$scope.optionQuoteToggled = function (item) {

        //    if (item.selected === true) {
        //        $scope.items.push(item.QuoteId);
        //    } else {
        //        $scope.items.splice(item, 1);
        //    }
        //    $scope.itemss = {
        //        selected: "true",
        //        requestId: id,
        //        items: $scope.items
        //    };

        //    sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY] = JSON.stringify($scope.itemss);

        //        $('.num_selected').text($scope.itemss.items.length);
        //        if ($scope.itemss.items.length == 0) {
        //            $('.chk-btn-generate-quotation').prop('disabled', true);
        //            $('.chk-btn-deliver').prop('disabled', true);
        //        } else {
        //            $('.chk-btn-generate-quotation').prop('disabled', false);
        //            $('.chk-btn-deliver').prop('disabled', false);
        //        }
        //};

        $scope.optionQuoteToggled = function (item) {
            var getQuote = [];

            if (item.selected === true) {
                $scope.items.push(item.QuoteId);
            } else {
                getQuote.push(item.QuoteId);

                var i = $scope.items.length;

                while (i--) {
                    if (getQuote.indexOf($scope.items[i]) != -1) {
                        $scope.items.splice(i, 1);
                    }
                }
            }
            $scope.itemss = {
                selected: "true",
                requestId: id,
                items: $scope.items
            };
            sessionStorage[GENERATE_ITEMS_LOCAL_STORAGE_KEY] = JSON.stringify($scope.itemss);

            $('.num_selected').text($scope.itemss.items.length);
            if ($scope.itemss.items.length == 0) {
                $('.chk-btn-generate-quotation').prop('disabled', true);
                $('.chk-btn-deliver').prop('disabled', true);
            } else {
                $('.chk-btn-generate-quotation').prop('disabled', false);
                $('.chk-btn-deliver').prop('disabled', false);
            }

        };

        $scope.hasProblem = function (record_status) {
            return ["PV", "PQ"].indexOf(record_status) !== -1;
        };
        $scope.showdelivery_status = true;
        $scope.delivery_status = false;
        $scope.generate = function () {
            $state.go("app.orders.order.generate");
        };

        $scope.deliver = function () {
            $state.go("app.orders.order.deliver");
        };

        angular.element(document).ready(function () {

            angular.element(".chk-btn-generate-quotation").on("click", function (e) {
                e.preventDefault();
                $scope.generate();
            });

            angular.element(".chk-btn-deliver").on("click", function (e) {
                e.preventDefault();
                $scope.deliver();

            });

        });
    });


angular.module("app").controller("accesscontrolCtrl", function ($state, $scope, $stateParams, ApiService, $window, $http, $sce) {

    // $http({
    //     method: 'GET',
    //     url: 'json/accesscontrol.json'
    // }).then(function(response) {
    //     $scope.ssoaGroup = response;

    // });
    getGrouAccess();

    function getGrouAccess() {
        ApiService.GetGroupAccess().then(
            function (r) {
                $scope.accessData = r.data;

            },
            function (error) {
                console.log(error);
            }
        );
    }


    $scope.accessControlModal = function (index1, index2) {
        $('#accessControlConfirmation' + index1 + index2).modal('show');
    }

    $scope.updateAccessControl = function (mId, gId, access) {

        //$('#accessControlConfirmation'+index).modal('hide').on("hidden.bs.modal", function() {
        $scope.formData = {
            MenuId: mId,
            GroupId: gId,
            IsAccessible: access
        }
        ApiService.UpdateGroupAccess($scope.formData).then(
            function (r) {

                getGrouAccess();
            },
            function (error) {
                console.log(error);
            }
        );
        //});

    }

    $scope.resetAccessControl = function () {
        getGrouAccess();
    }

});

angular.module("app").controller("faqCtrl", function ($scope, $location, $stateParams, ApiService, $timeout, $window) {
    $scope.$parent.showNav = true;

    $scope.initialId = {
        FAQId: null
    }
    initFaq();

    function initFaq() {
        ApiService.GetDraftFAQs()
            .then(function (r) {
                if (r.statusText === 'OK') {
                    $scope.initFaqs = r.data.FAQs[0];
                    $scope.faqGroupList = r.data.FAQs;
                    $scope.initialId.FAQId = $scope.initFaqs.FAQId;
                    $scope.faqGroup = "";

                    //GetDraftFAQs($scope.initialId);

                } else {

                }

            });
    }

    $scope.addGroupError = false;
    $scope.formDataGroups = {
        //FAQId: "",
        FAQId: null,
        Sequence: "",
        Description: ""
    };
    $scope.createFAQ = function () {
        if ($scope.formDataGroups.Description) {
            $scope.addGroupError = false;
            ApiService.CreateFAQ($scope.formDataGroups)
                .then(function (r) {
                    if (r.statusText === 'OK') {
                        $scope.formDataGroups = {
                            FAQId: null,
                            Sequence: "",
                            Description: ""
                        };
                        initFaq();
                    } else {

                    }

                });
        } else {
            $scope.addGroupError = true;
        }

    }

    $scope.formDataItems = {
        Question: "",
        Answer: "",
        ItemSequence: "",
        FAQItemId: ""
    };
    $scope.questionError = false;
    $scope.answerError = false;
    $scope.CreateFAQItem = function () {
        if ($scope.formDataItems.Question && $scope.formDataItems.Answer) {
            $scope.questionError = false;
            $scope.answerError = false;

            $scope.addFaqId = {
                FAQId: $scope.faqGroup
            }
            $scope.extendJson = angular.extend({}, $scope.formDataItems, $scope.addFaqId);

            ApiService.CreateFAQItem($scope.extendJson)
                .then(function (r) {
                    if (r.statusText === 'OK') {
                        $scope.formDataItems = {
                            FAQId: $scope.faqGroup,
                            Question: "",
                            Answer: "",
                            ItemSequence: "",
                            FAQItemId: null
                        };
                        GetDraftFAQs($scope.selectedGroupID);
                    } else {

                    }

                });
        } else {
            if (!$scope.formDataItems.Question) {
                $scope.questionError = true;
            } else {
                $scope.questionError = false;
            }

            if (!$scope.formDataItems.Answer) {
                $scope.answerError = true;
            } else {
                $scope.answerError = false;
            }
        }


    }


    function GetDraftFAQs(updateData) {
        ApiService.GetDraftFAQs(updateData)
            .then(function (r) {
                if (r.statusText === 'OK') {
                    $scope.faqs = r.data.FAQs[0];
                    $scope.descForDraft = $scope.faqs.Description;

                }

            });
    }

    $scope.faqGroupSelect = function (id) {
        $scope.faqGroup = id;
        $scope.selectedGroupID = {
            FAQId: id
        }

        GetDraftFAQs($scope.selectedGroupID);
    }

    $scope.sortableOptions = {
        items: '.sortable-item',
        'ui-floating': true,
        axis: "y",
        revert: true,
        scroll: false,
        placeholder: "sortable-placeholder",
        cursor: "move",
        update: function (e, ui) {
            console.log(e, ui);
            var dragIndex = ui.item.sortable.index + 1;
            var dropIndex = ui.item.sortable.dropindex + 1;
            var FAQItemId = ui.item.sortable.model.FAQItemId;
            var FAQId = ui.item.sortable.model.FAQId;

            setTimeout(function () {
                //console.log($scope.faqs.FAQItems);

                //$scope.faqs.FAQItems.forEach(function(item, index){
                $scope.formGroupDataItems = {
                    FAQId: FAQId,
                    FAQItemId: FAQItemId,
                    ItemSequence: dropIndex
                };
                ApiService.CreateFAQItem($scope.formGroupDataItems)
                    .then(function (r) {
                        if (r.statusText === 'OK') {
                            //console.log($scope.formDataItems);

                        } else {

                        }

                    });

                //});

            }, 500);


        },

    };

    $scope.sortableGroupOptions = {
        items: '.sortable-item',
        'ui-floating': true,
        axis: "y",
        revert: true,
        scroll: false,
        placeholder: "sortable-placeholder",
        cursor: "move",
        update: function (e, ui) {

            var count = 0;
            setTimeout(function () {

                $scope.faqGroupList.forEach(function (item, index) {
                    count = count + 1;
                    $scope.formDataGroupsSequence = {
                        Description: item.Description,
                        Sequence: count,
                        FAQId: item.FAQId,
                    };
                    ApiService.CreateFAQ($scope.formDataGroupsSequence)
                        .then(function (r) {
                            if (r.statusText === 'OK') {
                                //console.log($scope.formDataItems);

                            } else {

                            }

                        });

                });

            }, 500);


        }
    };

    $scope.faqEdit = function (faqData) {
        $scope.editDataItems = {
            FAQId: $scope.faqGroup,
            FAQItemId: faqData.FAQItemId,
            Question: faqData.Question,
            Answer: faqData.Answer,
            ItemSequence: faqData.ItemSequence
        };
        ApiService.CreateFAQItem($scope.editDataItems)
            .then(function (r) {
                if (r.statusText === 'OK') {
                    GetDraftFAQs($scope.selectedGroupID);
                } else {

                }

            });
    }

    $scope.faqGroupEdit = function (faqData) {
        $scope.editGroupDataItems = {
            Description: faqData.Description,
            Sequence: faqData.Sequence,
            FAQId: faqData.FAQId,
        };
        ApiService.CreateFAQ($scope.editGroupDataItems)
            .then(function (r) {
                if (r.statusText === 'OK') {
                    initFaq();
                } else {

                }

            });
    }

    $scope.cancelGroupEdit = function () {
        initFaq();
    }

    $scope.cancelEdit = function () {
        GetDraftFAQs($scope.selectedGroupID);
    }

    $scope.deleteModal = function (id) {
        $scope.deletion = {
            FAQId: null,
            FAQItemId: id
        }
        $('#confirmDeleteRequest').modal('show');
    }
    $scope.deleteGroupModal = function (id) {
        $scope.deletionGroup = {
            FAQId: id
        }
        $('#confirmDeleteGroupRequest').modal('show');
    }

    $scope.deleteFAQ = function () {

        ApiService.DeleteFAQ($scope.deletion)
            .then(function (r) {
                if (r.statusText === 'OK') {
                    GetDraftFAQs($scope.selectedGroupID);
                    $('#confirmDeleteRequest').modal('hide');
                } else {

                }

            });
    }


    $scope.deleteGroupFAQ = function () {

        ApiService.DeleteFAQ($scope.deletionGroup)
            .then(function (r) {
                if (r.statusText === 'OK') {
                    initFaq();
                    $('#confirmDeleteGroupRequest').modal('hide');
                } else {

                }

            });
    }

    var faqDraftArray = localStorage[FAQ_DRAFT_STORAGE_KEY] ? JSON.parse(localStorage[FAQ_DRAFT_STORAGE_KEY]) : [];
    $scope.getDraft = faqDraftArray;
    $scope.saveAsDraft = function () {

        $scope.draftItems = {
            FAQId: $scope.faqGroup,
            GroupName: $scope.descForDraft,
            DraftDate: moment().format()

        }

        //var draftLength = localStorage[FAQ_DRAFT_STORAGE_KEY] ? JSON.parse(localStorage[FAQ_DRAFT_STORAGE_KEY]).length : 0;
        $scope.extendDraftJson = angular.extend({}, $scope.formDataItems, $scope.draftItems);
        faqDraftArray.push($scope.extendDraftJson);

        localStorage[FAQ_DRAFT_STORAGE_KEY] = JSON.stringify(faqDraftArray);

        $scope.formDataItems = {
            FAQId: $scope.faqGroup,
            Question: "",
            Answer: "",
            ItemSequence: "",
            FAQItemId: null
        };

    }

    $scope.CreateDraftItem = function (index, id, form) {

        // $scope.addDraftFaqId = {
        //     FAQId: id
        // }
        // $scope.extendJson = angular.extend({}, $scope.draft, $scope.addDraftFaqId);

        ApiService.CreateFAQItem(form)
            .then(function (r) {
                if (r.statusText === 'OK') {

                    faqDraftArray.splice(index, 1);
                    localStorage[FAQ_DRAFT_STORAGE_KEY] = JSON.stringify(faqDraftArray);

                    faqDraftArray = JSON.parse(localStorage[FAQ_DRAFT_STORAGE_KEY]);
                    $scope.getDraft = faqDraftArray;

                    initFaq();
                    //     $scope.formDataItems = {
                    //         FAQId: $scope.faqGroup,
                    //         Question: "",
                    //         Answer: "",
                    //         ItemSequence: "",
                    //         FAQItemId: null
                    //     };
                    //     GetDraftFAQs($scope.selectedGroupID);
                    // } else {

                }

            });
    }

    $scope.refreshFaqGroup = function () {
        initFaq();
    }

    $scope.deleteDraftModal = function (key) {
        $scope.deleteDraftKey = key;
        $('#confirmDeleteDraftRequest').modal('show');
    }

    $scope.deleteDraft = function () {
        faqDraftArray.splice($scope.deleteDraftKey, 1);
        localStorage[FAQ_DRAFT_STORAGE_KEY] = JSON.stringify(faqDraftArray);

        faqDraftArray = JSON.parse(localStorage[FAQ_DRAFT_STORAGE_KEY]);
        $scope.getDraft = faqDraftArray;
        $('#confirmDeleteDraftRequest').modal('hide');
    }

    $scope.publishFaq = function () {
        $('#confirmPublish').modal('show');
    }

    $scope.confirmPublish = function () {
        ApiService.PublishFAQ()
            .then(function (r) {
                if (r.statusText === 'OK') {

                    $('#confirmPublish').modal('hide');
                    $scope.orderChangeStatus = true;
                    angular.element('.toastBox').addClass('active');
                    $timeout(function () {
                        angular.element('.toastBox').removeClass('active');
                    }, 2000);
                    initFaq();

                } else {
                    $('#confirmPublish').modal('hide');
                    $scope.orderChangeStatus = false;
                    angular.element('.toastBox').addClass('active');
                    $timeout(function () {
                        angular.element('.toastBox').removeClass('active');
                    }, 2000);
                }

            });
    }

    $scope.cancelAddGroup = function () {
        $scope.formDataGroups = {
            //FAQId: "",
            FAQId: null,
            Sequence: "",
            Description: ""
        };
    }

    $scope.cancelAddItem = function () {
        $scope.formDataItems = {
            FAQId: $scope.faqGroup,
            Question: "",
            Answer: "",
            ItemSequence: "",
            FAQItemId: null
        };
    }

});

angular.module("app").controller("emailTemplateCtrl", function ($scope, $location, $stateParams, ApiService, $timeout, $window) {
    $scope.$parent.showNav = true;
    initEmailTemplate();

    function initEmailTemplate() {
        ApiService.GetEmailContent()
            .then(function (r) {
                if (r.data.Status === 'OK') {
                    $scope.data = r.data;
                }
            });
    }

    $scope.getEmailBody = function (index, id) {
        $scope.emailID = {
            "EmailId": id
        }

        ApiService.GetEmailContent($scope.emailID)
            .then(function (r) {
                if (r.data.Status === 'OK') {
                    $scope.emailBody = r.data;
                    $scope.emailBody = $scope.emailBody ? $scope.emailBody.EmailContents[0] : ""

                    $scope.data.EmailContents[index]['Body'] = $scope.emailBody.Body;
                }
            });
    }

    $scope.EmailTemplateEdit = function (id, formData) {
        var data = {
            "EmailId": id,
            "Body": formData.Body
        }
        ApiService.UpdateEmailContent(data)
            .then(function (r) {
                if (r.data.Status === 'OK') {
                    $scope.SaveSuccess = true;
                    $timeout(function () {
                        $scope.SaveSuccess = false;
                    }, 2000);
                }
            });
    }

});

angular.module("app").controller("lgtSidebarCtrl", function ($scope, ApiService, $state, $timeout, $window) {

    // var cur_tenantCode = sessionStorage[TENANT_CODE] ? JSON.parse(sessionStorage[TENANT_CODE]) : "";
    var prev_tenantCode = sessionStorage[PREV_TENANT_CODE] ? JSON.parse(sessionStorage[PREV_TENANT_CODE]) : "";
    var cur_tenantCode = sessionStorage[TENANT_CODE] ? JSON.parse(sessionStorage[TENANT_CODE]) : "";
    sessionStorage[PREV_TENANT_CODE] = JSON.stringify(cur_tenantCode);

    getTenant(cur_tenantCode);

    function getTenant(data) {
        ApiService.getaccess().then(
            function (r) {
                $scope.tenants = r.data.Tenants;

                $scope.selectedName = "";
                if ($scope.tenants.length > 0 && $scope.tenants.length <= 1) {

                    $scope.formData = {
                        TenantCode: r.data.Tenants[0]
                    }

                    if ($scope.formData.TenantCode) {
                        sessionStorage[TENANT_CODE] = JSON.stringify($scope.formData);
                        sessionStorage[TENANT_COUNT] = 1;
                        getSelectedTenant($scope.formData);
                    }
                } else {
                    if (data) {
                        sessionStorage[TENANT_COUNT] = $scope.tenants.length;
                        getSelectedTenant(data);
                    }
                }
            },
            function (error) {
                console.log(error);
            }
        );
    }

    function getSelectedTenant(data) {
        ApiService.getaccess(data).then(
            function (r) {
                $scope.selectedTenants = r.data.Tenants;
                $scope.selectedName = $scope.selectedTenants[0].Description;
                // $timeout(function() {

                if ($scope.tenants.length > 1 && prev_tenantCode.TenantCode != cur_tenantCode.TenantCode) {
                    $state.go($scope.selectedTenants[0].Menus[0].SubMenus[0].Url);
                }
                //}, 100);

            },
            function (error) {
                console.log(error);
            }
        );
    }

    $scope.selectTenant = function (selection) {
        $scope.formData = {
            TenantCode: selection
        }

        sessionStorage[TENANT_CODE] = JSON.stringify($scope.formData);
        $window.location.reload(true);
    }

    // $scope.menuItems = [{
    //         title: "Requests",
    //         href: "app.orders",
    //         icon: "fa fa-shopping-cart",
    //         subMenu: [{
    //             title: "Orders",
    //             href: "app.orders.list",
    //             blank: false
    //         }, {
    //             title: "Delivery",
    //             href: "app.orders.delivery.list",
    //             blank: false
    //         }]
    //     }, {
    //         title: "Catalog",
    //         href: "app.products",
    //         icon: "fa fa-book",
    //         subMenu: [{
    //             title: "Products",
    //             href: "app.products.list",
    //             blank: false
    //         }]
    //     }, {
    //         title: "Collection",
    //         href: "app.collections",
    //         icon: "fa fa-truck",
    //         subMenu: [{
    //             title: "Counter Collection",
    //             href: "app.collections.list",
    //             blank: false
    //         }]
    //     },
    //     // {

    //     //{
    //     //    title: "Promotions",
    //     //    href: "app.promotions",
    //     //    icon: "fa fa-tags",
    //     //    subMenu: [
    //     //        {
    //     //            title: "Management",
    //     //            href: "app.promotions.list",
    //     //            blank: false
    //     //        }
    //     //    ]
    //     //},
    //     //{
    //     //    title: "Configuration",
    //     //    href: "app.configuration",
    //     //    icon: "fa fa-cubes",
    //     //    subMenu: [
    //     //        {
    //     //            title: "Internal Notification",
    //     //            href: "app.configuration.notification.list",
    //     //            blank: false
    //     //        },

    //     //        {
    //     //            title: "Kiosk",
    //     //            href: "app.configuration.kiosk",
    //     //            blank: false
    //     //        },

    //     //    ]
    //     //},
    //     {
    //         title: "Report",
    //         href: "app.excel",
    //         icon: "fa fa-file-excel-o",
    //         subMenu: [{
    //             title: "Transaction Report",
    //             href: "app.excel.export",
    //             blank: false
    //         }, {
    //             title: "Order Report",
    //             href: "app.excel.orderReport",
    //             blank: false
    //         }, {
    //             title: "Delivery Report",
    //             href: "app.excel.deliveryReport",
    //             blank: false
    //         }]
    //     }

    // ];
});