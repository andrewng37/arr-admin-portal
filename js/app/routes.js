﻿(function() {
    angular
        .module("app.pages.configuration", [
            "ui.router",
            "app.pages.configuration.notification",
            "app.pages.configuration.kiosk"
        ])
        .config(routeConfig);
    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.configuration", {
                url: "/configuration",
                abstract: true,
                templateUrl: "views/pages/configuration/configuration_template.html",
                ncyBreadcrumb: {
                    label: "Configuration"
                }
            })
            .state("app.configuration.notification", {
                url: "/notification",
                // controller: "OrdersListCtrl",
                templateUrl: "views/pages/configuration/notification/notification.html",
                ncyBreadcrumb: {
                    label: "Notification"
                },
                params: {
                    subtitle: ""
                }
            }).state("app.configuration.kiosk", {
                url: "/kiosk",
                templateUrl: "views/pages/configuration/kiosk/kiosk.html",
                params: {},
                controller: "KioskCtrl",
                ncyBreadcrumb: {
                    label: "Kiosk Display"
                }
            }).state("app.configuration.accesscontrol", {
                url: "/accesscontrol",
                templateUrl: "views/pages/configuration/accesscontrol/accesscontrol.html",
                params: {},
                ncyBreadcrumb: {
                    label: "Access Control"
                }
            }).state("app.configuration.faq", {
                url: "/faq",
                templateUrl: "views/pages/configuration/FAQ/faq.html",
                params: {},
                ncyBreadcrumb: {
                    label: "FAQ Management"
                }
            }).state("app.configuration.emailtemplate", {
                url: "/emailtemplate",
                templateUrl: "views/pages/configuration/emailTemplate/emailtemplate.html",
                params: {},
                ncyBreadcrumb: {
                    label: "Email Template Management"
                }
            });
    }
})();



(function() {
    angular
        .module("app.pages.excel", [
            "ui.router",
            "app.pages.excel.export"
        ])
        .config(routeConfig);
    angular.module('app.pages.excel.export', [
        'ui.router',
        'ngCsv'
    ]);

    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.excel", {
                url: "/excel",
                abstract: true,
                templateUrl: "views/pages/excel/excel_template.html",
                ncyBreadcrumb: {
                    label: "Excel Import/Export"
                }
            }).state("app.excel.export", {
                url: "/export",
                controller: "excelExportCtrl",
                templateUrl: "views/pages/excel/export/export.html",
                ncyBreadcrumb: {
                    label: "Transaction Report"
                },
                params: {
                    subtitle: ""
                }
            }).state("app.excel.orderReport", {
                url: "/orderReport",
                controller: "excelOrderReportCtrl",
                templateUrl: "views/pages/excel/export/orderReport.html",
                ncyBreadcrumb: {
                    label: "Order Report"
                },
                params: {
                    subtitle: ""
                }
            }).state("app.excel.deliveryReport", {
                url: "/deliveryReport",
                controller: "excelDeliveryReportCtrl",
                templateUrl: "views/pages/excel/export/deliveryReport.html",
                ncyBreadcrumb: {
                    label: "Delivery Report"
                },
                params: {
                    subtitle: ""
                }
            })
    }
})();

(function() {
    angular
        .module("app.pages.orders", [
            "ui.router",
            "app.pages.orders.order",
            "app.pages.orders.delivery"
        ])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.orders", {
                url: "/orders",
                abstract: true,
                templateUrl: "views/pages/orders/orders_template.html",
                ncyBreadcrumb: {
                    label: "Order"
                }
            })
            .state("app.orders.list", {
                url: "/list",
                controller: "OrdersListCtrl",
                templateUrl: "views/pages/orders/list.html",
                ncyBreadcrumb: {
                    label: "Order Management"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("app.orders.delivery", {
                url: "/delivery",
                // controller: "OrdersListCtrl",
                templateUrl: "views/pages/orders/delivery/delivery.html",
                ncyBreadcrumb: {
                    label: "Delivery Management"
                },
                params: {
                    subtitle: ""
                }
            });
    }
})();

(function() {
    angular
        .module("app.pages.products", [
            "ui.router",
        ])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.products", {
                url: "/products",
                abstract: true,
                templateUrl: "views/pages/products/products_template.html",
                ncyBreadcrumb: {
                    label: "Product"
                }
            })
            .state("app.products.list", {
                url: "/list",
                controller: "ProductsListCtrl",
                templateUrl: "views/pages/products/list.html",
                ncyBreadcrumb: {
                    label: "Products Management"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("app.products.edit", {
                url: "/:id",
                controller: "ProductsEditCtrl",
                templateUrl: "views/pages/products/edit/edit_product.html",
                ncyBreadcrumb: {
                    label: "Edit Products Detail"
                },
                params: {
                    params: {
                        id: null
                    },
                }
            });

    }
})();


(function() {
    angular
        .module("app.pages.collections", [
            "ui.router",
        ])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.collections", {
                url: "/collections",
                abstract: true,
                templateUrl: "views/pages/collections/collections_template.html",
                ncyBreadcrumb: {
                    label: "Collections"
                }
            })
            .state("app.collections.list", {
                url: "/list",
                controller: "collectionsListCtrl",
                templateUrl: "views/pages/collections/list.html",
                ncyBreadcrumb: {
                    label: "Collections Management"
                },
                params: {
                    subtitle: ""
                }
            })

    }
})();

(function() {
    angular
        .module("app.pages.promotions", [
            "ui.router"
        ])
        .config(routeConfig);

    function routeConfig($stateProvider) {
        $stateProvider
            .state("app.promotions", {
                url: "/promotions",
                abstract: true,
                templateUrl: "views/pages/promotions/promotions_template.html",
                ncyBreadcrumb: {
                    label: "Promotions"
                }
            })
            .state("app.promotions.list", {
                url: "/list",
                controller: "PromotionsListCtrl",
                templateUrl: "views/pages/promotions/list.html",
                ncyBreadcrumb: {
                    label: "Promotion Management"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("app.promotions.add", {
                url: "/add",
                controller: "PromotionsAddCtrl",
                templateUrl: "views/pages/promotions/add.html",
                ncyBreadcrumb: {
                    label: "Add New Promotion Code"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("app.promotions.modify", {
                url: "/modify/:id",
                controller: "PromotionsModifyCtrl",
                templateUrl: "views/pages/promotions/modify.html",
                ncyBreadcrumb: {
                    label: "Modify Promotion Code"
                },
                params: {
                    id: null,
                    subtitle: ""
                }
            });
    }
})();

(function() {
    angular
        .module("app.pages.configuration.kiosk", [
            "ui.router",

        ])
        .config(routeConfig);

    function routeConfig(
        $stateProvider,
        $urlRouterProvider,
        $ocLazyLoadProvider
    ) {
        $ocLazyLoadProvider.config({
            debug: true
        });

        $urlRouterProvider.otherwise("A");

    }
})();

(function() {
    angular
        .module("app.pages.configuration.notification", [
            "ui.router",

            "app.pages.configuration.notification.list",
            "app.pages.configuration.notification.edit"
        ])
        .config(routeConfig);

    function routeConfig(
        $stateProvider,
        $urlRouterProvider,
        $ocLazyLoadProvider
    ) {
        $ocLazyLoadProvider.config({
            debug: true
        });

        $urlRouterProvider.otherwise("A");

        $stateProvider
            .state("app.configuration.notification.list", {
                url: "/list",
                templateUrl: "views/pages/configuration/notification/list/list-notification.html",
                controller: "NotificationCtrl",
                ncyBreadcrumb: {
                    label: "List Notification"
                },
                params: {}
            })
            .state("app.configuration.notification.edit", {
                url: "/edit/:id",
                templateUrl: "views/pages/configuration/notification/edit/edit-notification.html",
                controller: "NotiEditCtrl",
                ncyBreadcrumb: {
                    label: "Edit Notification"
                },
                params: {
                    id: null
                }
            });
    }
})();

(function() {
    angular
        .module("app.pages.orders.delivery", [
            "ui.router",
            "app.pages.orders.delivery.list",
            "app.pages.orders.delivery.edit"
        ])
        .config(routeConfig);

    function routeConfig(
        $stateProvider,
        $urlRouterProvider,
        $ocLazyLoadProvider
    ) {
        $ocLazyLoadProvider.config({
            debug: true
        });

        $urlRouterProvider.otherwise("A");

        $stateProvider
            .state("app.orders.delivery.list", {
                url: "/list",
                templateUrl: "views/pages/orders/delivery/list/list.html",
                controller: "DeliveryCtrl",
                ncyBreadcrumb: {
                    label: "Delivery Management"
                },
                params: {}
            })
            .state("app.orders.delivery.edit", {
                url: "/edit/:id/:code1",
                templateUrl: "views/pages/orders/delivery/edit/edit-delivery.html",
                controller: "DeliveryEditCtr",
                ncyBreadcrumb: {
                    label: "Delivery Instruction Detail"
                },
                params: {
                    id: null,
                    code1: null
                }
            })
            .state("app.orders.delivery.deliver", {
                url: "/delivery/deliver",
                templateUrl: "views/pages/orders/delivery/deliver/deliver.html",
                controller: "deliverCtrl",
                ncyBreadcrumb: {
                    label: "Generate"
                },
                params: {
                    obj: null,
                    subtitle: ""
                }
            });
    }
})();

(function() {
    angular
        .module("app.pages.orders.order", [
            "ui.router",
            "app.pages.orders.order.product",
            "app.pages.orders.order.info",
            "app.pages.orders.order.log",
            "app.pages.orders.order.edit",
            "app.pages.orders.order.generate",
            "app.pages.orders.order.deliver"
        ])
        .config(routeConfig);
    angular.module('app.pages.orders.order.log', [
        'ui.router'
    ]);

    function routeConfig(
        $stateProvider,
        $urlRouterProvider,
        $ocLazyLoadProvider
    ) {
        $ocLazyLoadProvider.config({
            debug: true
        });

        $urlRouterProvider.otherwise("A");

        $stateProvider
            .state("app.orders.order", {
                abstract: true,
                url: "/:id/:code1/:code2",
                templateUrl: "views/pages/orders/order/order_template.html",
                params: {
                    id: null,
                    code1: null,
                    code2: null
                },
                controller: "OrderCtrl",
                ncyBreadcrumb: {
                    label: "Order"
                }
            })
            .state("app.orders.order.product", {
                url: "/product",
                templateUrl: "views/pages/orders/order/product/product.html",
                controller: "OrderProductCtrl",
                ncyBreadcrumb: {
                    label: "Order Detail"
                },
                params: {
                    //id: null,
                    subtitle: ""
                }
            })
            .state("app.orders.order.info", {
                url: "/info",
                templateUrl: "views/pages/orders/order/info/info.html",
                controller: "OrderInfoCtrl",
                ncyBreadcrumb: {
                    label: "Info"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("taxInvoices", {
                url: "/taxInvoices/:id/:code2",
                templateUrl: "views/pages/orders/order/info/taxInvoices.html",
                controller: "TaxInvoicesCtrl",
                params: {
                    //id: null,
                    subtitle: ""
                }
            })
            .state("app.orders.order.log", {
                url: "/log",
                templateUrl: "views/pages/orders/order/log/log.html",
                controller: "OrderLogCtrl",
                ncyBreadcrumb: {
                    label: "Log"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("app.orders.order.edit", {
                url: "/edit/:item_id",
                templateUrl: "views/pages/orders/order/edit/edit.html",
                controller: "OrderEditCtr",
                ncyBreadcrumb: {
                    label: "Edit"
                },
                params: {
                    item_Id: null
                }
            })
            .state("app.orders.order.generate", {
                url: "/generate/generate-quotation",
                templateUrl: "views/pages/orders/order/generate/generateQuotation.html",
                controller: "OrderGenerateCtr",
                ncyBreadcrumb: {
                    label: "Generate"
                },
                params: {
                    subtitle: ""
                }
            })
            .state("app.orders.order.deliver", {
                url: "/generate/generate-deliver/",
                templateUrl: "views/pages/orders/order/deliver/deliver.html",
                controller: "deliverCtrl",
                ncyBreadcrumb: {
                    label: "Generate Deliver"
                },
                params: {
                    subtitle: ""

                }
            });
    }
})();

(function() {

    angular.module('app.pages.orders.order.product', [
            'ui.router',
        ])
        .config(routeConfig);

    function routeConfig($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {

    }

})();

(function() {
    "use strict";

    angular
        .module("app.pages", [
            "ui.router",
            "app.pages.orders",
            "app.pages.products",
            "app.pages.collections",
            "app.pages.promotions",
            "app.pages.configuration",
            "app.pages.excel"
        ])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($urlRouterProvider, $stateProvider) {
        //$urlRouterProvider.otherwise("/dashboard");

        //$stateProvider.state("app.sample", {
        //  url: "/sample",
        //  templateUrl: "views/pages/sample.html",
        //  ncyBreadcrumb: {
        //    label: "Sample"
        //  }
        //});
    }
})();