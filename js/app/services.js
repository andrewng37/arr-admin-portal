﻿
angular
    .module("app")
    .factory("DataService", [function () {

        var _data = {};

        return {
            get: function (key) {
                return _data[key];
            },
            set: function (key, value) {
                _data[key] = value;
            }
        }
    }]).
    factory('getItemsList', [function () {
        var items = [];
        return {
            get: function () {
                return items;
            },
            set: function (list) {
                items = list;
            },
            value: items
        };
        //var check = {
        //    Value: false
        //};
        //return check;
    }]).
    factory("ApiService", ["$http", function ($http) {
        var method = "POST";
        var base = "//localhost:8889/arr_api/";
        var config = {
            CheckAdminSession: base + "SessionApi/CheckAdminSession",
            GetOrder: base + "RequestApi/GetOrder",
            GetOrder1: base + "RequestApi/GetOrder1",
            getStatus: base + "RequestApi/GetNextStatus",
            UpdateStatus: base + "RequestApi/UpdateStatus",
            RegisterDelivery: base + "DeliveryApi/GenerateDeliveryOrders",
            GenerateInvoice: base + "InvoiceApi/GenerateInvoice",
            AttachmentUploads: base + "AttachmentApi/Upload",
            DeleteAttachment: base + "AttachmentApi/DeleteAttachment",
            UpdateOrderItem: base + "RequestApi/UpdateOrderItem",
            GetOrderStatus: base + "RequestApi/GetOrderStatus",
            GetCategory: base + "RequestApi/GetCategory",
            UpdateOrderItemCategoryRoute: base + "RequestApi/UpdateOrderItemCategoryRoute",
            GetEmailTemplate: base + "EmailApi/GetEmailTemplate",
            CancelOrderItem: base + "CancelApi/CancelOrderItem",
            GetDeliveryMethods: base + "DeliveryApi/GetDeliveryMethods",
            GetDelivery: base + "DeliveryApi/GetDelivery",
            AdminLogOff: base + "SessionApi/AdminLogOff",
            GetTransDetail: base + "ReportApi/GetTransDetail",
            GetTransSummary: base + "ReportApi/GetTransSummary",
            GetPriceRule: base  + "RequestApi/GetPriceRule",
            GetPaymentTransaction: base  + "ReportApi/GetPaymentTransaction",
            Attachment: base + "DeliveryApi/Attachment",
            GetA20Files: base + "AttachmentApi/GetA20Files",
            GetPayment: base + "RequestApi/GetPayment",
            GetOrderDetails: base + "requestapi/GetOrderDetails",
            GetTaxInvoice: base + "Requestapi/GetTaxInvoice",
            GetOrderItems: base + "RequestApi/GetOrderItems",
            GetOrderSummary: base + "ReportApi/GetOrderSummary",
            GetOrderDetail: base + "ReportApi/GetOrderDetail",
            GetAttachDownloadLog: base + "ReportApi/GetAttachDownloadLog",
            UpdateDelivery: base + "DeliveryApi/UpdateDelivery",
            ViewPersonalData: base + "RequestApi/ViewPersonalData",
            getworkorder: base + "RequestApi/getworkorder",
            GetWorkOrderItem: base + "RequestApi/GetWorkOrderItem",
            CompleteOrderItems: base + "RequestApi/CompleteOrderItems",
            GetDeliveryReport: base + "ReportApi/GetDeliveryReport",
            GetDeliveryCollection: base + "DeliveryApi/GetDeliveryCollection",
            getaccess: base + "sessionapi/getaccess",
            GetGroupAccess: base + "GroupAccessApi/GetGroupAccess",
            UpdateGroupAccess: base + "GroupAccessApi/UpdateGroupAccess",
            CreateFAQItem: base + "FAQApi/CreateFAQItem",
            CreateFAQ: base + "FAQApi/CreateFAQ",
            GetFAQs: base + "FAQApi/GetFAQs",
            DeleteFAQ: base + "FAQApi/DeleteFAQ",
            PublishFAQ: base + "FAQApi/PublishFAQ",
            GetDraftFAQs: base + "FAQApi/GetDraftFAQs",
            GetEmailContent: base + "EmailApi/GetEmailContent",
            UpdateEmailContent: base + "EmailApi/UpdateEmailContent"
        };
        return {
            GetOrder: function (requestData) {
                return $http({ "url": config.GetOrder, "data": requestData, "method": method });
            },
            GetOrder1: function (requestData) {
                return $http({ "url": config.GetOrder1, "data": requestData, "method": method });
            },
            CheckAdminSession: function () {
                return $http({ "url": config.CheckAdminSession, "method": method });
            },
            GetTransDetail: function (requestData) {
                return $http({ "url": config.GetTransDetail, "data": requestData, "method": method });
            },
            GetOrderItems: function (requestData) {
                return $http({ "url": config.GetOrderItems, "data": requestData, "method": method });
            },
            GetTransSummary: function (requestData) {
                return $http({ "url": config.GetTransSummary, "data": requestData, "method": method });
            },
            GetPaymentTransaction: function (requestData) {
                return $http({ "url": config.GetPaymentTransaction, "data": requestData, "method": method });
            },
            GetOrderSummary: function (reportList) {
                return $http({ "url": config.GetOrderSummary, "data": reportList, "method": method });
            },
            GetOrderDetail: function (reportList) {
                return $http({ "url": config.GetOrderDetail, "data": reportList, "method": method });
            },
            GetAttachDownloadLog: function (reportList) {
                return $http({ "url": config.GetAttachDownloadLog, "data": reportList, "method": method });
            },
            getStatus: function (requestData) {
                return $http({ "url": config.getStatus, "data": requestData, "method": method });
            },
            GetPriceRule: function () {
                return $http({ "url": config.GetPriceRule, "method": method });
            },
            UpdateStatus: function (requestData) {
                return $http({ "url": config.UpdateStatus, "data": requestData, "method": method });
            },
            RegisterDelivery: function (requestData) {
                return $http({ "url": config.RegisterDelivery, "data": requestData, "method": method });
            },
            UpdateDelivery: function (requestData) {
                return $http({ "url": config.UpdateDelivery, "data": requestData, "method": method });
            },
            Attachment: function (data) {
                return $.ajax({
                    "url": config.Attachment,
                    "type": method,
                    "data": data,
                    "cache": false,
                    "contentType": false,
                    "processData": false,

                });
            },
            GetA20Files: function (requestData) {
                return $http({ "url": config.GetA20Files, "params": requestData, "method": method });
            },
            GenerateInvoice: function (requestData) {
                return $http({ "url": config.GenerateInvoice, "data": requestData, "method": method });
            },
            UpdateOrderItem: function (requestData) {
                return $http({ "url": config.UpdateOrderItem, "data": requestData, "method": method });
            },
            GetOrderStatus: function (requestData) {
                return $http({ "url": config.GetOrderStatus, "data": requestData, "method": method });
            },
            GetDelivery: function (requestData) {
                return $http({ "url": config.GetDelivery, "data": requestData, "method": method });
            },
            GetOrderDetails: function (requestData) {
                return $http({ "url": config.GetOrderDetails, "data": requestData, "method": method });
            },
            AttachmentUploads: function (data) {

                return $.ajax({
                    "url": config.AttachmentUploads,
                    "type": method,
                    "data": data,
                    "cache": false,
                    "contentType": false,
                    "processData": false,
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with upload progress
                            }
                        }, false);
                        //Download progress
                        xhr.addEventListener("progress", function (evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                //Do something with download progress

                            }
                        }, false);
                        return xhr;
                    },
                });
            },
            DeleteAttachment: function (requestData) {
                return $http({ "url": config.DeleteAttachment, "data": requestData, "method": method });
            },
            GetCategory: function () {
                return $http({ "url": config.GetCategory, "data": "", "method": method });
            },
            UpdateOrderItemCategoryRoute: function (requestData) {
                return $http({ "url": config.UpdateOrderItemCategoryRoute, "data": requestData, "method": method });
            },
            GetEmailTemplate: function (req) {
                return $http({ "url": config.GetEmailTemplate, "data": req, "method": method });
            },
            CancelOrderItem: function (req) {
                return $http({ "url": config.CancelOrderItem, "data": req, "method": method });
            },
            GetDeliveryMethods: function () {
                return $http({ "url": config.GetDeliveryMethods, "method": method });
            },
            GetPayment: function (req) {
                return $http({ "url": config.GetPayment, "data": req, "method": method });
            },
            GetTaxInvoice: function (req) {
                return $http({ "url": config.GetTaxInvoice, "data": req, "method": method });
            },
            ViewPersonalData: function (req) {
                return $http({ "url": config.ViewPersonalData, "data": req, "method": method });
            },
            getworkorder: function (req) {
                return $http({ "url": config.getworkorder, "data": req, "method": method });
            },
            GetWorkOrderItem: function (req) {
                return $http({ "url": config.GetWorkOrderItem, "data": req, "method": method });
            },
            CompleteOrderItems: function (req) {
                return $http({ "url": config.CompleteOrderItems, "data": req, "method": method });
            },
            GetDeliveryReport: function (req) {
                return $http({ "url": config.GetDeliveryReport, "data": req, "method": method });
            },
            GetDeliveryCollection: function (req) {
                return $http({ "url": config.GetDeliveryCollection, "data": req, "method": method });
            },
            getaccess: function (req) {
                return $http({ "url": config.getaccess, "data": req, "method": method });
            },
            GetGroupAccess: function () {
                return $http({ "url": config.GetGroupAccess, "method": method });
            },
            UpdateGroupAccess: function (req) {
                return $http({ "url": config.UpdateGroupAccess, "data": req, "method": method });
            },
            CreateFAQ: function(formData) {
                return $http({
                    "url": config.CreateFAQ,
                    "data": formData,
                    "method": method
                });
            },
            CreateFAQItem: function(formData) {
                return $http({
                    "url": config.CreateFAQItem,
                    "data": formData,
                    "method": method
                });
            },
            GetFAQs: function(formData) {
                return $http({
                    "url": config.GetFAQs,
                    "data": formData,
                    "method": method
                });
            },
            DeleteFAQ: function(formData) {
                return $http({
                    "url": config.DeleteFAQ,
                    "data": formData,
                    "method": method
                });
            },
            PublishFAQ: function(formData) {
                return $http({
                    "url": config.PublishFAQ,
                    "data": formData,
                    "method": method
                });
            },
            GetDraftFAQs: function(formData) {
                return $http({
                    "url": config.GetDraftFAQs,
                    "data": formData,
                    "method": method
                });
            },
            GetEmailContent: function(formData) {
                return $http({
                    "url": config.GetEmailContent,
                    "data": formData,
                    "method": method
                });
            },
            UpdateEmailContent: function(formData) {
                return $http({
                    "url": config.UpdateEmailContent,
                    "data": formData,
                    "method": method
                });
            },
        }
    }])
    .filter("durationFormat", [function () {
        return function (input) {
            var output = "";
            input = input.split(":");

            while (input.length > 0) {
                var cnt = input.length;
                var tmp = parseInt(input.shift());

                if (tmp != 0) {
                    output += " " + tmp;
                    switch (cnt) {
                        case 3: output += " hr" + (tmp > 1 ? "s" : ""); break;
                        case 2: output += " min" + (tmp > 1 ? "s" : ""); break;
                        case 1: output += " sec" + (tmp > 1 ? "s" : ""); break;
                    }
                }
            }
            return $.trim(output) || "0 sec";
        }
    }])
    .filter("timeFormat", [function () {
        return function (input) {
            var output = "";
            var first = true;
            input = input.split(":");

            while (input.length > 0) {
                var cnt = input.length;
                var tmp = input.shift();

                if (parseInt(tmp) != 0 || !first || input.length <= 1) {
                    first = false;
                    output = output + (output ? ":" : "") + tmp;
                }

            }
            return $.trim(output) || "00:00";
        }
    }])
    .filter("currencyFormat", [function () {
        return function (input) {
            var val = Math.round(Number(input) * 100) / 100;
            var parts = val.toString().split(".");
            var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "." + (+parts[1] ? parts[1] + (+parts[1] > 9 ? "" : "0") : "00");

            return num + " SGD";
        }
    }])
    .filter('unique', function () {

        return function (collection, keyname) {
            var output = [],
                keys = [];
            angular.forEach(collection, function (item) {
                var key = item[keyname];
                if (keys.indexOf(key) === -1) {
                    keys.push(key);
                    output.push(item);
                }
            });
            return output;
        };
    }).filter('secondsToDateTime', function () {
        return function (seconds) {
            var d = new Date(0, 0, 0, 0, 0, 0, 0);
            d.setSeconds(seconds);
            return d;
        };
    }).filter('groupBy', [
        '$parse',
        'pmkr.filterStabilize',
        function ($parse, filterStabilize) {

            function groupBy(input, prop) {

                if (!input) { return; }

                var grouped = {};

                input.forEach(function (item) {
                    var key = $parse(prop)(item);
                    grouped[key] = grouped[key] || [];
                    grouped[key].push(item);
                });

                return grouped;

            }

            return filterStabilize(groupBy);

        }]).factory('pmkr.filterStabilize', [
            'pmkr.memoize',
            function (memoize) {

                function service(fn) {

                    function filter() {
                        var args = [].slice.call(arguments);
                        // always pass a copy of the args so that the original input can't be modified
                        args = angular.copy(args);
                        // return the `fn` return value or input reference (makes `fn` return optional)
                        var filtered = fn.apply(this, args) || args[0];
                        return filtered;
                    }

                    var memoized = memoize(filter);

                    return memoized;

                }

                return service;

            }
        ])

    .factory('pmkr.memoize', [
        function () {

            function service() {
                return memoizeFactory.apply(this, arguments);
            }

            function memoizeFactory(fn) {

                var cache = {};

                function memoized() {

                    var args = [].slice.call(arguments);

                    var key = JSON.stringify(args);

                    if (cache.hasOwnProperty(key)) {
                        return cache[key];
                    }

                    cache[key] = fn.apply(this, arguments);

                    return cache[key];

                }

                return memoized;

            }

            return service;

        }
    ])
    .service('anchorSmoothScroll', function () {

        this.scrollTo = function (eID) {

            var startY = currentYPosition();
            var stopY = elmYPosition(eID);
            var distance = stopY > startY ? stopY - startY : startY - stopY;
            if (distance < 100) {
                scrollTo(0, stopY); return;
            }
            var speed = Math.round(distance / 100);
            if (speed >= 20) speed = 20;
            var step = Math.round(distance / 25);
            var leapY = stopY > startY ? startY + step : startY - step;
            var timer = 0;
            if (stopY > startY) {
                for (var i = startY; i < stopY; i += step) {
                    setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                    leapY += step; if (leapY > stopY) leapY = stopY; timer++;
                } return;
            }
            for (var i = startY; i > stopY; i -= step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
            }

            function currentYPosition() {
                // Firefox, Chrome, Opera, Safari
                if (self.pageYOffset) return self.pageYOffset;
                // Internet Explorer 6 - standards mode
                if (document.documentElement && document.documentElement.scrollTop)
                    return document.documentElement.scrollTop;
                // Internet Explorer 6, 7 and 8
                if (document.body.scrollTop) return document.body.scrollTop;
                return 0;
            }

            function elmYPosition(eID) {
                var elm = document.getElementById(eID);
                var y = elm.offsetTop;
                var node = elm;
                while (node.offsetParent && node.offsetParent != document.body) {
                    node = node.offsetParent;
                    y += node.offsetTop;
                } return y;
            }

        };

    });
//    .directive('disableOnClick', function () {
//    function link(scope, element, attrs) {
//        scope.isDesabled = false;
//        element.bind('click', () => {
//            element[0].disabled = true;
//        });
//    }

//    return {
//        restrict: 'A',
//        link: link
//    };
//});
